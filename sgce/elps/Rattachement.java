// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.modele.ElpId;

public class Rattachement extends ElpId
{
    private String nom;
    
    public Rattachement(final int id, final String nom) {
        super(id);
        this.nom = nom;
    }
    
    public String getNom() {
        return this.nom;
    }
}
