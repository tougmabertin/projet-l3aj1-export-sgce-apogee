// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import java.util.ArrayList;
import sgce.modele.ElpId;

public class Departement extends ElpId
{
    private String nom;
    private String acronyme;
    private ArrayList<Mention> listM;
    
    public Departement(final int id, final String nom, final String acronyme) {
        super(id);
        this.listM = new ArrayList<Mention>();
        this.nom = nom;
        this.acronyme = acronyme;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getAcronyme() {
        return this.acronyme;
    }
    
    public void addMentionToList(final Mention mention) {
        this.listM.add(mention);
    }
    
    public ArrayList<Mention> getListeMention() {
        return this.listM;
    }
}
