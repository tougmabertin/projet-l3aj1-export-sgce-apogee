// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import java.util.LinkedHashMap;
import sgce.modele.ElpId;

public class Ecue extends ElpId
{
    private String nom;
    private String code;
    private String acronyme;
    private String cnu;
    private SemEnum semEnum;
    private float ects;
    private LinkedHashMap<String, String> heuresCours;
    private BlocSousChoix blocSousChoix;
    
    public Ecue(final int id, final String nom, final String code, final String acronyme, final SemEnum semEnum, final String cnu, final float ects) {
        super(id);
        this.heuresCours = new LinkedHashMap<String, String>();
        this.nom = nom;
        this.code = code;
        this.acronyme = acronyme;
        this.semEnum = semEnum;
        this.cnu = cnu;
        this.ects = ects;
        this.heuresCours.put("CM", "");
        this.heuresCours.put("TD", "");
        this.heuresCours.put("TP", "");
        this.heuresCours.put("Projet", "");
        this.heuresCours.put("Stage", "");
        this.heuresCours.put("TDM", "");
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public String getAcronyme() {
        return this.acronyme;
    }
    
    public SemEnum getSemestre() {
        return this.semEnum;
    }
    
    public String getCnu() {
        return this.cnu;
    }
    
    public float getEcts() {
        return this.ects;
    }
    
    public void addHeureCours(final String type, final String heures) {
        this.heuresCours.put(type, heures);
    }
    
    public LinkedHashMap<String, String> getHeuresCours() {
        return this.heuresCours;
    }
    
    public void setBlocSousChoix(final BlocSousChoix bsc) {
        this.blocSousChoix = bsc;
    }
    
    public BlocSousChoix getBlocSousChoix() {
        return this.blocSousChoix;
    }
}
