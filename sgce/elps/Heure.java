// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.modele.ElpId;

public class Heure extends ElpId
{
    private Ecue ecue;
    private TypeCours type;
    private float heures;
    
    public Heure(final int id, final Ecue ecue, final TypeCours type, final float heures) {
        super(id);
        this.ecue = ecue;
        this.type = type;
        this.heures = heures;
    }
    
    public Ecue getEcue() {
        return this.ecue;
    }
    
    public TypeCours getType() {
        return this.type;
    }
    
    public float getHeures() {
        return this.heures;
    }
}
