// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.liste.ListeElps;
import java.util.LinkedHashMap;
import sgce.modele.ElpId;

public class Ue extends ElpId
{
    private String nom;
    private String code;
    private String cnu;
    private float ects;
    private SemEnum semEnum;
    private BlocChoix blocChoix;
    private LinkedHashMap<String, String> heuresCours;
    private ListeElps<BlocSousChoix> listeBsc;
    
    public Ue(final int id, final String nom, final String code, final float ects, final String cnu, final SemEnum semEnum) {
        super(id);
        this.heuresCours = new LinkedHashMap<String, String>();
        this.listeBsc = new ListeElps<BlocSousChoix>();
        this.nom = nom;
        this.code = code;
        this.ects = ects;
        this.cnu = cnu;
        this.semEnum = semEnum;
        this.heuresCours.put("CM", "");
        this.heuresCours.put("TD", "");
        this.heuresCours.put("TP", "");
        this.heuresCours.put("Projet", "");
        this.heuresCours.put("Stage", "");
        this.heuresCours.put("TDM", "");
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public float getEcts() {
        return this.ects;
    }
    
    public String getCnu() {
        return this.cnu;
    }
    
    public SemEnum getSemestre() {
        return this.semEnum;
    }
    
    public void setBlocChoix(final BlocChoix bc) {
        this.blocChoix = bc;
    }
    
    public BlocChoix getBlocChoix() {
        return this.blocChoix;
    }
    
    public void addBscToListe(final BlocSousChoix bsc) {
        this.listeBsc.addToList(bsc);
        this.listeBsc.setTypeListe(Obligatoire.O);
        this.listeBsc.setCode("");
        this.listeBsc.setNom("");
    }
    
    public ListeElps<BlocSousChoix> getListeBsc() {
        return this.listeBsc;
    }
    
    public void addHeureCours(final String type, final String heures) {
        this.heuresCours.put(type, heures);
    }
    
    public LinkedHashMap<String, String> getHeuresCours() {
        return this.heuresCours;
    }
}
