// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.liste.Semestre;
import sgce.liste.ListeElps;
import sgce.modele.ElpId;

public class Parcours extends ElpId
{
    private String nom;
    private String code;
    private Formation formation;
    private float ects;
    private ListeElps<Semestre> listeSemestre;
    
    public Parcours(final int id, final Formation formation, final String nom, final String code) {
        super(id);
        this.listeSemestre = new ListeElps<Semestre>();
        this.nom = nom;
        this.code = code;
        this.formation = formation;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public Formation getFormation() {
        return this.formation;
    }
    
    public void addSemestreToList(final Semestre sem) {
        sem.setNom(this.formation.getTypeFormation());
        sem.setCodePere(new Parcours(this.getId(), this.formation, this.nom, this.code));
        this.listeSemestre.addToList(sem);
        this.listeSemestre.setTypeListe(Obligatoire.O);
        this.listeSemestre.setCode("");
        this.listeSemestre.setNom("");
    }
    
    public ListeElps<Semestre> getListeSemestre() {
        return this.listeSemestre;
    }
    
    public void setEcts(final float ects) {
        this.ects = ects;
    }
    
    public float getEcts() {
        return this.ects;
    }
}
