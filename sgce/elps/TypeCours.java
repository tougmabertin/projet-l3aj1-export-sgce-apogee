// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.modele.ElpId;

public class TypeCours extends ElpId
{
    private String intitule;
    
    public TypeCours(final int id, final String intitule) {
        super(id);
        this.intitule = intitule;
    }
    
    public String getIntitule() {
        return this.intitule;
    }
}
