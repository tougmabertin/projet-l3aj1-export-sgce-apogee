// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.liste.ListeElps;
import sgce.modele.ElpId;

public class BlocChoix extends ElpId
{
    private Parcours parcours;
    private int totalUes;
    private SemEnum semEnum;
    private String nom;
    private String code;
    private float ects;
    private float nbUes;
    private Obligatoire obligatoire;
    private ListeElps<Ue> listeUe;
    
    public BlocChoix(final int id, final Parcours parcours, final Obligatoire obligatoire, final float nbUes, final int totalUes, final String nom, final String code, final SemEnum semEnum, final float ects) {
        super(id);
        this.listeUe = new ListeElps<Ue>();
        this.parcours = parcours;
        this.obligatoire = obligatoire;
        this.nbUes = nbUes;
        this.totalUes = totalUes;
        this.nom = nom;
        this.code = code;
        this.semEnum = semEnum;
        this.ects = ects;
    }
    
    public Parcours getParcours() {
        return this.parcours;
    }
    
    public Obligatoire getObligatoire() {
        return this.obligatoire;
    }
    
    public int getNbUes() {
        return (int)this.nbUes;
    }
    
    public int getTotalUes() {
        return this.totalUes;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public SemEnum getSemestre() {
        return this.semEnum;
    }
    
    public float getEcts() {
        return this.ects;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void addUeToListe(final Ue ue) {
        this.listeUe.addToList(ue);
        this.listeUe.setCode(this.code);
        this.listeUe.setNom(this.nom);
        this.listeUe.setTypeListe(this.obligatoire);
    }
    
    public ListeElps<Ue> getListeUe() {
        return this.listeUe;
    }
}
