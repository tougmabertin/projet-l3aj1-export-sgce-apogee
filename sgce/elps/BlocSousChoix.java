// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import sgce.liste.ListeElps;
import sgce.modele.ElpId;

public class BlocSousChoix extends ElpId
{
    private Ue ue;
    private int nbEcues;
    private int totalEcues;
    private String nom;
    private ListeElps<Ecue> listeEcue;
    
    public BlocSousChoix(final int id, final Ue ue, final int nbEcues, final int totalEcues, final String nom) {
        super(id);
        this.listeEcue = new ListeElps<Ecue>();
        this.ue = ue;
        this.nbEcues = nbEcues;
        this.totalEcues = totalEcues;
        this.nom = nom;
    }
    
    public Ue getUe() {
        return this.ue;
    }
    
    public int getNbEcues() {
        return this.nbEcues;
    }
    
    public int getTotalEcues() {
        return this.totalEcues;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public Obligatoire getObligatoire() {
        final Obligatoire obligatoire = (this.totalEcues - this.nbEcues == 0) ? Obligatoire.O : ((this.totalEcues - this.nbEcues != 0) ? Obligatoire.OC : Obligatoire.N);
        return obligatoire;
    }
    
    public void addEcueToListe(final Ecue ecue) {
        this.listeEcue.addToList(ecue);
        this.listeEcue.setTypeListe(this.getObligatoire());
        this.listeEcue.setCode("");
        this.listeEcue.setNom("");
    }
    
    public ListeElps<Ecue> getListeEcue() {
        return this.listeEcue;
    }
}
