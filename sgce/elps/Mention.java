// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import java.util.ArrayList;
import sgce.modele.ElpId;

public class Mention extends ElpId
{
    private String libelle;
    private String code;
    private ArrayList<Formation> listF;
    private Departement departement;
    private TypeMention type;
    
    public Mention(final int id, final String libelle, final TypeMention type, final String code) {
        super(id);
        this.listF = new ArrayList<Formation>();
        this.libelle = libelle;
        this.code = code;
        this.type = type;
    }
    
    public String getLibelle() {
        return this.libelle;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public TypeMention getType() {
        return this.type;
    }
    
    public void addFormationToList(final Formation formation) {
        this.listF.add(formation);
    }
    
    public ArrayList<Formation> getListeFormation() {
        return this.listF;
    }
    
    public void setDepartement(final Departement dept) {
        this.departement = dept;
    }
    
    public Departement getDepartement() {
        return this.departement;
    }
}
