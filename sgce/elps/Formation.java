// 
// Decompiled by Procyon v0.5.36
// 

package sgce.elps;

import java.util.ArrayList;
import sgce.modele.ElpId;

public class Formation extends ElpId
{
    private String nom;
    private String code;
    private String acronyme;
    private int version;
    private Mention mention;
    private ArrayList<Parcours> listP;
    private TypeFormation typeForm;
    
    public Formation(final int id, final String nom, final String acronyme, final String code, final int version, final Mention mention) {
        super(id);
        this.listP = new ArrayList<Parcours>();
        this.nom = nom;
        this.acronyme = acronyme;
        this.code = code;
        this.version = version;
        this.mention = mention;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public String getAcronyme() {
        return this.acronyme;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public Mention getMention() {
        return this.mention;
    }
    
    public void addParcourstoListe(final Parcours parcours) {
        this.listP.add(parcours);
    }
    
    public ArrayList<Parcours> getListeParcours() {
        return this.listP;
    }
    
    public void setTypeFormation(final TypeFormation type) {
        this.typeForm = type;
    }
    
    public TypeFormation getTypeFormation() {
        return this.typeForm;
    }
}
