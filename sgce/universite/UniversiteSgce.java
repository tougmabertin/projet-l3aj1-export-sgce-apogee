// 
// Decompiled by Procyon v0.5.36
// 

package sgce.universite;

import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import sgce.elps.BlocSousChoix;
import sgce.elps.BlocChoix;
import sgce.elps.Heure;
import sgce.elps.TypeCours;
import sgce.elps.Ecue;
import sgce.elps.Ue;
import sgce.elps.Parcours;
import sgce.elps.Formation;
import sgce.elps.Mention;
import sgce.elps.Departement;
import sgce.elps.Rattachement;
import java.util.Map;

public class UniversiteSgce
{
    private Map<Integer, Rattachement> rattachements;
    private Map<Integer, Departement> departements;
    private Map<Integer, Mention> mentions;
    private Map<Integer, Formation> formations;
    private Map<Integer, Parcours> parcours;
    private Map<Integer, Ue> ues;
    private Map<Integer, Ecue> ecues;
    private Map<Integer, TypeCours> typesCours;
    private Map<Integer, Heure> heures;
    private Map<Integer, BlocChoix> blocsChoix;
    private Map<Integer, BlocSousChoix> blocsSousChoix;
    
    public UniversiteSgce() {
        this.rattachements = new HashMap<Integer, Rattachement>();
        this.departements = new HashMap<Integer, Departement>();
        this.mentions = new HashMap<Integer, Mention>();
        this.formations = new HashMap<Integer, Formation>();
        this.parcours = new HashMap<Integer, Parcours>();
        this.ues = new HashMap<Integer, Ue>();
        this.ecues = new HashMap<Integer, Ecue>();
        this.typesCours = new HashMap<Integer, TypeCours>();
        this.heures = new HashMap<Integer, Heure>();
        this.blocsChoix = new HashMap<Integer, BlocChoix>();
        this.blocsSousChoix = new HashMap<Integer, BlocSousChoix>();
    }
    
    public void addRattachement(final Rattachement rattachement) {
        this.rattachements.put(rattachement.getId(), rattachement);
    }
    
    public Rattachement getRattacementById(final int id) {
        return this.rattachements.get(id);
    }
    
    public ArrayList<Rattachement> getTousLesRattachements() {
        final ArrayList<Rattachement> listR = new ArrayList<Rattachement>(this.rattachements.values());
        return listR;
    }
    
    public void addDepartement(final Departement dept) {
        this.departements.put(dept.getId(), dept);
    }
    
    public Departement getDepartementById(final int id) {
        return this.departements.get(id);
    }
    
    public ArrayList<Departement> getTousLesDepartements() {
        final ArrayList<Departement> listD = new ArrayList<Departement>(this.departements.values());
        return listD;
    }
    
    public void addMention(final Mention mention) {
        this.mentions.put(mention.getId(), mention);
    }
    
    public Mention getMentionById(final int id) {
        return this.mentions.get(id);
    }
    
    public ArrayList<Mention> getToutesLesMentions() {
        final ArrayList<Mention> listM = new ArrayList<Mention>(this.mentions.values());
        return listM;
    }
    
    public void addFormation(final Formation formation) {
        this.formations.put(formation.getId(), formation);
    }
    
    public Formation getFormationById(final int id) {
        return this.formations.get(id);
    }
    
    public ArrayList<Formation> getToutesLesFormations() {
        final ArrayList<Formation> listF = new ArrayList<Formation>(this.formations.values());
        return listF;
    }
    
    public void addParcours(final Parcours parc) {
        this.parcours.put(parc.getId(), parc);
    }
    
    public Parcours getParcoursById(final int id) {
        return this.parcours.get(id);
    }
    
    public ArrayList<Parcours> getTousLesParcours() {
        final ArrayList<Parcours> listP = new ArrayList<Parcours>(this.parcours.values());
        return listP;
    }
    
    public void addUe(final Ue ue) {
        this.ues.put(ue.getId(), ue);
    }
    
    public Ue getUeById(final int id) {
        return this.ues.get(id);
    }
    
    public ArrayList<Ue> getToutesLesUes() {
        final ArrayList<Ue> listUe = new ArrayList<Ue>(this.ues.values());
        return listUe;
    }
    
    public void addEcue(final Ecue ecue) {
        this.ecues.put(ecue.getId(), ecue);
    }
    
    public Ecue getEcueById(final int id) {
        return this.ecues.get(id);
    }
    
    public ArrayList<Ecue> getToutesLesEcues() {
        final ArrayList<Ecue> listEcue = new ArrayList<Ecue>(this.ecues.values());
        return listEcue;
    }
    
    public void addBlocChoix(final BlocChoix bc) {
        this.blocsChoix.put(bc.getId(), bc);
    }
    
    public BlocChoix getBlocChoixById(final int id) {
        return this.blocsChoix.get(id);
    }
    
    public ArrayList<BlocChoix> getAllBlocsChoix() {
        final ArrayList<BlocChoix> listBc = new ArrayList<BlocChoix>(this.blocsChoix.values());
        return listBc;
    }
    
    public void addBlocSousChoix(final BlocSousChoix bsc) {
        this.blocsSousChoix.put(bsc.getId(), bsc);
    }
    
    public BlocSousChoix getBlocSousChoixById(final int id) {
        return this.blocsSousChoix.get(id);
    }
    
    public ArrayList<BlocSousChoix> getAllBlocsSousChoix() {
        final ArrayList<BlocSousChoix> listBsc = new ArrayList<BlocSousChoix>(this.blocsSousChoix.values());
        return listBsc;
    }
    
    public void addTypeCours(final TypeCours type) {
        this.typesCours.put(type.getId(), type);
    }
    
    public TypeCours getTypeCoursById(final int id) {
        return this.typesCours.get(id);
    }
    
    public ArrayList<TypeCours> getAllTypesCours() {
        final ArrayList<TypeCours> listTc = new ArrayList<TypeCours>(this.typesCours.values());
        return listTc;
    }
    
    public void addHeures(final Heure h) {
        this.heures.put(h.getId(), h);
    }
    
    public Heure getHeureById(final int id) {
        return this.heures.get(id);
    }
    
    public ArrayList<Heure> getAllHeures() {
        final ArrayList<Heure> listH = new ArrayList<Heure>(this.heures.values());
        return listH;
    }
}
