// 
// Decompiled by Procyon v0.5.36
// 

package sgce.main;

import java.io.IOException;
import java.sql.SQLException;
import sgce.elps.Formation;
import pdf.ComparaisonPdf;
import transformation.SgceToApogee;
import apogee.csvreader.CsvApogeeReader;
import apogee.universite.UniversiteApogee;
import sgce.bd.Dao;
import sgce.bd.Bd;
import sgce.universite.UniversiteSgce;

public class Main
{
    public static void main(final String[] args) throws SQLException, ClassNotFoundException, IOException {
        final UniversiteSgce universite = new UniversiteSgce();
        final Dao dao = new Dao(new Bd("configBDD.properties"));
        dao.putAllDepartements(universite);
        dao.putAllMentions(universite);
        dao.putAllFormations(universite);
        dao.putAllParcours(universite);
        dao.putAllBlocsChoix(universite);
        dao.putAllUes(universite);
        dao.putAllBlocsSousChoix(universite);
        dao.putAllEcues(universite);
        dao.putAllTypesCours(universite);
        dao.putAllHeures(universite);
        dao.putRelationMD(universite);
        dao.putRelationUeBC(universite);
        dao.putRelationEcueBSC(universite);
        final String dir = "src/main/resources/";
        final String sourceVet = String.valueOf(dir) + "Vets.csv";
        final String sourceElp = String.valueOf(dir) + "Elps.csv";
        final String sourceHeure = String.valueOf(dir) + "Heures.csv";
        final UniversiteApogee universiteApogee1 = new UniversiteApogee();
        final CsvApogeeReader read = new CsvApogeeReader();
        read.putAllFormations(universiteApogee1, sourceVet);
        read.putAllElps(universiteApogee1, sourceElp, sourceVet);
        read.putAllListesApogee(universiteApogee1, sourceElp, sourceVet);
        read.putAllHeure(universiteApogee1, sourceHeure);
        final UniversiteApogee universiteApogee2 = new UniversiteApogee();
        final SgceToApogee transform = new SgceToApogee(universiteApogee2);
        final Formation formSgce = universite.getFormationById(5);
        final apogee.formation.Formation formApogee1 = universiteApogee1.getFormationByCode("MLG4C1");
        final apogee.formation.Formation formApogee2 = transform.transformationFormation(formSgce);
        final ComparaisonPdf compare = new ComparaisonPdf(String.valueOf(dir) + "/comparaison.pdf", universiteApogee1, universiteApogee2);
        compare.createPdf(formApogee1, formApogee2);
    }
}
