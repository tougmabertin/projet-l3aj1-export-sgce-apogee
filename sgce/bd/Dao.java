// 
// Decompiled by Procyon v0.5.36
// 

package sgce.bd;

import sgce.elps.Heure;
import sgce.elps.TypeCours;
import sgce.elps.Ecue;
import sgce.elps.BlocSousChoix;
import sgce.elps.Ue;
import java.util.Iterator;
import sgce.elps.BlocChoix;
import sgce.liste.Semestre;
import sgce.elps.Parcours;
import sgce.elps.Formation;
import sgce.elps.Mention;
import sgce.elps.Departement;
import java.sql.ResultSet;
import java.sql.Statement;
import sgce.elps.Rattachement;
import sgce.universite.UniversiteSgce;
import sgce.elps.TypeFormation;
import sgce.elps.Obligatoire;
import sgce.elps.TypeMention;
import sgce.elps.SemEnum;
import java.sql.SQLException;
import java.sql.Connection;

public class Dao
{
    private int annee;
    private Bd bd;
    
    public Dao(final Bd bd) {
        this.annee = 2019;
        this.bd = bd;
    }
    
    public Connection getConnection() throws SQLException {
        return this.bd.getConnection();
    }
    
    public SemEnum getSemestreFromInt(final int sem) {
        final SemEnum semEnum = (sem == 2) ? SemEnum.S2 : ((sem == 1) ? SemEnum.S1 : SemEnum.AN);
        return semEnum;
    }
    
    public TypeMention getTypeMentionFromString(final String type) {
        final TypeMention typeM = type.equals("L") ? TypeMention.L : (type.equals("LP") ? TypeMention.LP : TypeMention.M);
        return typeM;
    }
    
    public Obligatoire getTypeBloc(final String oblig, final float nb_ue, final int total_ue) {
        final Obligatoire obligatoire = (oblig.equals("O") && total_ue - nb_ue == 0.0f) ? Obligatoire.O : ((oblig.equals("O") && total_ue - nb_ue != 0.0f) ? Obligatoire.OC : (oblig.equals("F") ? Obligatoire.F : Obligatoire.N));
        return obligatoire;
    }
    
    public TypeFormation getTypeFormationFromInt(final int type) {
        final TypeFormation typeForm = (type == 1) ? TypeFormation.L1 : ((type == 2) ? TypeFormation.L2 : ((type == 3) ? TypeFormation.L3 : ((type == 4) ? TypeFormation.LP3 : ((type == 5) ? TypeFormation.M1 : ((type == 6) ? TypeFormation.M2 : TypeFormation.NR)))));
        return typeForm;
    }
    
    public void putAllRattachements(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from rattachement");
                    try {
                        while (rs.next()) {
                            universite.addRattachement(new Rattachement(rs.getInt("id"), rs.getString("nom")));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllDepartements(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from departements");
                    try {
                        while (rs.next()) {
                            universite.addDepartement(new Departement(rs.getInt("id"), rs.getString("nom"), rs.getString("acronyme")));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllMentions(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from mention");
                    try {
                        while (rs.next()) {
                            final TypeMention type = this.getTypeMentionFromString(rs.getString("type_mention"));
                            universite.addMention(new Mention(rs.getInt("id"), rs.getString("libelle"), type, rs.getString("codeapogee")));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllFormations(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from formation_" + this.annee);
                    try {
                        while (rs.next()) {
                            final Formation form = new Formation(rs.getInt("id"), rs.getString("nom_apogee"), rs.getString("acronyme"), rs.getString("codeapogee"), rs.getInt("version_apogee"), universite.getMentionById(rs.getInt("mentionid")));
                            form.setTypeFormation(this.getTypeFormationFromInt(rs.getInt("typeformationid")));
                            universite.addFormation(form);
                            final Mention mention = universite.getMentionById(rs.getInt("mentionid"));
                            if (mention != null && !mention.getListeFormation().contains(form)) {
                                mention.addFormationToList(form);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllParcours(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from parcours_" + this.annee);
                    try {
                        while (rs.next()) {
                            final Parcours parcours = new Parcours(rs.getInt("id"), universite.getFormationById(rs.getInt("id_formation")), rs.getString("nom"), rs.getString("code_apogee"));
                            universite.addParcours(parcours);
                            parcours.setEcts(rs.getFloat("ects_s1") + rs.getFloat("ects_s2"));
                            final Semestre s1 = new Semestre(SemEnum.S1);
                            s1.setEcts(rs.getFloat("ects_s1"));
                            final Semestre s2 = new Semestre(SemEnum.S2);
                            s2.setEcts(rs.getFloat("ects_s2"));
                            parcours.addSemestreToList(s1);
                            parcours.addSemestreToList(s2);
                            final Formation form = universite.getFormationById(rs.getInt("id_formation"));
                            if (form != null && !form.getListeParcours().contains(parcours)) {
                                form.addParcourstoListe(parcours);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllBlocsChoix(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from choix_" + this.annee);
                    try {
                        while (rs.next()) {
                            final String oblig = rs.getString("obligatoire");
                            final Obligatoire obligatoire = this.getTypeBloc(oblig, rs.getFloat("nb_ue"), rs.getInt("total_ue"));
                            final int sem = rs.getInt("semestre");
                            final SemEnum semEnum = this.getSemestreFromInt(sem);
                            final BlocChoix bc = new BlocChoix(rs.getInt("id"), universite.getParcoursById(rs.getInt("id_parcours")), obligatoire, rs.getFloat("nb_ue"), rs.getInt("total_ue"), rs.getString("nom"), rs.getString("code_apogee"), semEnum, rs.getFloat("ects"));
                            universite.addBlocChoix(bc);
                            for (final Semestre semestre : universite.getParcoursById(rs.getInt("id_parcours")).getListeSemestre().getListe()) {
                                semestre.addBcToList(bc);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllUes(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from ue_" + this.annee);
                    try {
                        while (rs.next()) {
                            final int sem = rs.getInt("semestre");
                            final SemEnum semEnum = this.getSemestreFromInt(sem);
                            universite.addUe(new Ue(rs.getInt("id"), rs.getString("nom"), rs.getString("code_apogee"), rs.getFloat("ects"), rs.getString("cnu"), semEnum));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllBlocsSousChoix(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from sous_choix_" + this.annee);
                    try {
                        while (rs.next()) {
                            final BlocSousChoix bsc = new BlocSousChoix(rs.getInt("id"), universite.getUeById(rs.getInt("id_ue")), rs.getInt("nb_modules"), rs.getInt("total_modules"), rs.getString("nom"));
                            universite.addBlocSousChoix(bsc);
                            final Ue ue = universite.getUeById(rs.getInt("id_ue"));
                            if (ue != null && !ue.getListeBsc().getListe().contains(bsc)) {
                                ue.addBscToListe(bsc);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllEcues(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from module_" + this.annee);
                    try {
                        while (rs.next()) {
                            final int sem = rs.getInt("semestre");
                            final SemEnum semEnum = this.getSemestreFromInt(sem);
                            universite.addEcue(new Ecue(rs.getInt("id"), rs.getString("nom"), rs.getString("codeapogee"), rs.getString("acronyme"), semEnum, rs.getString("CNU"), rs.getFloat("ECTS")));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllTypesCours(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from typecours");
                    try {
                        while (rs.next()) {
                            universite.addTypeCours(new TypeCours(rs.getInt("id"), rs.getString("intitule")));
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putAllHeures(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from poste_" + this.annee);
                    try {
                        while (rs.next()) {
                            final float h = rs.getFloat("heures");
                            final Heure heure = new Heure(rs.getInt("id"), universite.getEcueById(rs.getInt("moduleid")), universite.getTypeCoursById(rs.getInt("typeid")), h);
                            universite.addHeures(heure);
                            final Ecue ecue = universite.getEcueById(rs.getInt("moduleid"));
                            final TypeCours type = universite.getTypeCoursById(rs.getInt("typeid"));
                            if (ecue != null && type != null) {
                                ecue.addHeureCours(type.getIntitule(), Float.toString(h));
                                for (final Ue ue : universite.getToutesLesUes()) {
                                    if (ue.getCode().equals(ecue.getCode())) {
                                        ue.addHeureCours(type.getIntitule(), Float.toString(h));
                                    }
                                }
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putRelationMD(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from formation_" + this.annee);
                    try {
                        while (rs.next()) {
                            final Mention mention = universite.getMentionById(rs.getInt("mentionid"));
                            final Departement dept = universite.getDepartementById(rs.getInt("accreditation_departementid"));
                            if (mention != null && dept != null) {
                                if (!dept.getListeMention().contains(mention)) {
                                    dept.addMentionToList(mention);
                                }
                                mention.setDepartement(dept);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putRelationUeBC(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from relation_choix_ue_" + this.annee);
                    try {
                        while (rs.next()) {
                            final Ue ue = universite.getUeById(rs.getInt("id_ue"));
                            final BlocChoix bc = universite.getBlocChoixById(rs.getInt("id_choix"));
                            if (ue != null && bc != null) {
                                ue.setBlocChoix(bc);
                                if (bc.getListeUe().getListe().contains(ue)) {
                                    continue;
                                }
                                bc.addUeToListe(ue);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
    
    public void putRelationEcueBSC(final UniversiteSgce universite) throws SQLException {
        Throwable t = null;
        try {
            final Connection connection = this.getConnection();
            try {
                final Statement ps = connection.createStatement();
                try {
                    final ResultSet rs = ps.executeQuery("SELECT * from relation_sous_choix_module_" + this.annee);
                    try {
                        while (rs.next()) {
                            final Ecue ecue = universite.getEcueById(rs.getInt("id_module"));
                            final BlocSousChoix bsc = universite.getBlocSousChoixById(rs.getInt("id_sous_choix"));
                            if (ecue != null && bsc != null) {
                                ecue.setBlocSousChoix(bsc);
                                if (bsc.getListeEcue().getListe().contains(ecue)) {
                                    continue;
                                }
                                bsc.addEcueToListe(ecue);
                            }
                        }
                    }
                    finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (ps != null) {
                        ps.close();
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
                if (connection != null) {
                    connection.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception3;
                t = exception3;
            }
            else {
                final Throwable exception3;
                if (t != exception3) {
                    t.addSuppressed(exception3);
                }
            }
        }
    }
}
