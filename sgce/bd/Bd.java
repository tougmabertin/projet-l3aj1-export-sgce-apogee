// 
// Decompiled by Procyon v0.5.36
// 

package sgce.bd;

import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;

public class Bd
{
    private Properties properties;
    
    public Bd(final String fileName) throws IOException, ClassNotFoundException {
        this.properties = new Properties();
        Throwable t = null;
        try {
            final InputStream input = new FileInputStream(fileName);
            try {
                this.properties.load(input);
            }
            finally {
                if (input != null) {
                    input.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception;
                t = exception;
            }
            else {
                final Throwable exception;
                if (t != exception) {
                    t.addSuppressed(exception);
                }
            }
        }
        Class.forName(this.properties.getProperty("driver"));
    }
    
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(this.properties.getProperty("url"), this.properties.getProperty("login"), this.properties.getProperty("password"));
    }
}
