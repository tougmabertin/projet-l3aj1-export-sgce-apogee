// 
// Decompiled by Procyon v0.5.36
// 

package sgce.liste;

import sgce.elps.Obligatoire;
import sgce.elps.Parcours;
import sgce.elps.TypeFormation;
import sgce.elps.SemEnum;
import sgce.elps.BlocChoix;

public class Semestre
{
    private float ects;
    private String nom;
    private String codePere;
    private ListeElps<BlocChoix> listeBc;
    private SemEnum sem;
    
    public Semestre(final SemEnum sem) {
        this.listeBc = new ListeElps<BlocChoix>();
        this.sem = sem;
    }
    
    public SemEnum getSemEnum() {
        return this.sem;
    }
    
    public void setNom(final TypeFormation type) {
        if (type == TypeFormation.L1 || type == TypeFormation.M1) {
            if (this.sem == SemEnum.S1) {
                this.nom = "Semestre 1";
            }
            else if (this.sem == SemEnum.S2) {
                this.nom = "Semestre 2";
            }
            else {
                this.nom = "Non reconnu";
            }
        }
        else if (type == TypeFormation.L2 || type == TypeFormation.M2) {
            if (this.sem == SemEnum.S1) {
                this.nom = "Semestre 3";
            }
            else if (this.sem == SemEnum.S2) {
                this.nom = "Semestre 4";
            }
            else {
                this.nom = "Non reconnu";
            }
        }
        else if (type == TypeFormation.L3 || type == TypeFormation.LP3) {
            if (this.sem == SemEnum.S1) {
                this.nom = "Semestre 5";
            }
            else if (this.sem == SemEnum.S2) {
                this.nom = "Semestre 6";
            }
            else {
                this.nom = "Non reconnu";
            }
        }
        else {
            this.nom = "Non reconnu";
        }
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public void setCodePere(final Parcours parcours) {
        if (parcours.getFormation().getListeParcours().size() <= 1) {
            this.codePere = parcours.getFormation().getCode();
        }
        else {
            this.codePere = parcours.getCode();
        }
    }
    
    public void setCodePere(final String codeP) {
        this.codePere = codeP;
    }
    
    public String getCodePere() {
        return this.codePere;
    }
    
    public void addBcToList(final BlocChoix bc) {
        if (bc.getSemestre() == this.sem) {
            this.listeBc.addToList(bc);
            this.listeBc.setTypeListe(Obligatoire.O);
            this.listeBc.setNom("");
            this.listeBc.setCode("");
        }
    }
    
    public ListeElps<BlocChoix> getListeBc() {
        return this.listeBc;
    }
    
    public void setEcts(final float ects) {
        this.ects = ects;
    }
    
    public float getEcts() {
        return this.ects;
    }
}
