// 
// Decompiled by Procyon v0.5.36
// 

package sgce.liste;

import java.util.ArrayList;
import sgce.elps.Obligatoire;

public class ListeElps<E>
{
    private Obligatoire type;
    private String nom;
    public String code;
    private ArrayList<E> liste;
    
    public ListeElps() {
        this.liste = new ArrayList<E>();
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    public void setNom(final String nom) {
        this.nom = nom;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public void addToList(final E elp) {
        this.liste.add(elp);
    }
    
    public ArrayList<E> getListe() {
        return this.liste;
    }
    
    public void setTypeListe(final Obligatoire oblig) {
        this.type = oblig;
    }
    
    public Obligatoire getTypeListe() {
        return this.type;
    }
}
