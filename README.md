# Projet L3AJ1 Export SGCE-Apogee


**Contexte :**
SGCE est un logiciel permettant de faire une modélisation et une estimation prévisionnelle du coût des formations de l’université. Apogée est le logiciel utilisé pour les inscriptions et la saisie des notes des étudiants. La modélisation des formations dans Apogée découle directement de la modélisation des informations dans SGCE.

**Objectifs :******
L’objectif est de produire des documents indiquant les paramétrages à réaliser dans Apogée en fonction de la modélisation SGCE.
- vérifier a postériori la conformité de la saisie apogée avec la modélisation SGCE
- Travail à réaliser :
- récupération d’objets java à partir de la base de données SGCE
- déduction des objets Apogée
- élaboration d’une documentation à destination des référents apogée
- récupération d’objets java à partir de csv issus d’Apogée
- vérification de la conformité avec production d’un document détaillant les différences constatées

Ce projet est susceptible de pouvoir se poursuivre par un stage au siège de l’université. Le projet sera réalisé en Java et utilisera maven pour les tests unitaires et la journalisation. Le dépôt svn est utilisé dès le début du projet par tous les membres de l'équipe.

**Langage(s) et technologie(s)**:
Java, JUnit, JDBC, iText, CSV
