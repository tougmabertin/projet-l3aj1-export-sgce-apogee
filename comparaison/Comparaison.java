// 
// Decompiled by Procyon v0.5.36
// 

package comparaison;

import apogee.Heure.Heure;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import apogee.liste.ListeApogee;
import apogee.elp.ElpApogee;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import apogee.formation.Formation;
import apogee.universite.UniversiteApogee;

public class Comparaison
{
    private UniversiteApogee univ1;
    private UniversiteApogee univ2;
    
    public Comparaison(final UniversiteApogee univ1, final UniversiteApogee univ2) {
        this.univ1 = univ1;
        this.univ2 = univ2;
    }
    
    public List<String> afficherDiffFormation(final Formation form1, final Formation form2) {
        final List<String> diffParams = new ArrayList<String>();
        if (form1 != null && form2 != null) {
            diffParams.add(this.compareParam(form1.getCodeEtp(), form2.getCodeEtp()));
            diffParams.add(this.compareNumericParam(form1.getVersion(), form2.getVersion()));
            diffParams.add(this.compareParam(form1.getNom(), form2.getNom()));
            diffParams.add(this.compareParam(form1.getAcronyme(), form2.getAcronyme()));
        }
        return Collections.unmodifiableList((List<? extends String>)diffParams);
    }
    
    public void getDescendantsFormation(final Formation form, final List<ElpApogee> listeElps, final List<ListeApogee> listeLses) {
        if (form != null) {
            for (final ListeApogee lse : form.getListe()) {
                listeLses.add(lse);
                this.getDescendantsListeApogee(lse, listeElps, listeLses);
            }
        }
    }
    
    public List<String> afficheDiffLse(final ListeApogee lse1, final ListeApogee lse2) {
        final List<String> diffParams = new ArrayList<String>();
        if (lse1 != null && lse2 != null) {
            diffParams.add(this.compareParam(lse1.getCodeLse(), lse2.getCodeLse()));
            diffParams.add(this.compareParam(lse1.getType().toString(), lse2.getType().toString()));
            diffParams.add(this.compareParam(lse1.getNom(), lse2.getNom()));
            diffParams.add(this.compareNumericParam(lse1.getNbreChoixMin(), lse2.getNbreChoixMax()));
        }
        return Collections.unmodifiableList((List<? extends String>)diffParams);
    }
    
    public void getDescendantsListeApogee(final ListeApogee lse, final List<ElpApogee> listeElps, final List<ListeApogee> listeLses) {
        if (lse != null) {
            for (final ElpApogee elp : lse.getListe()) {
                listeElps.add(elp);
                this.getDescendantsElp(elp, listeElps, listeLses);
            }
        }
    }
    
    public List<String> getSharedLses(final List<ListeApogee> listeLses1, final List<ListeApogee> listeLses2, final List<String> missingsLses, final List<String> addedLses) {
        final List<String> listeCodes1 = this.getListOfLsesCodes(listeLses1);
        final List<String> listeCodes2 = this.getListOfLsesCodes(listeLses2);
        missingsLses.addAll(listeCodes2);
        addedLses.addAll(listeCodes1);
        missingsLses.removeAll(listeCodes1);
        addedLses.removeAll(listeCodes2);
        final List<String> sharedLses = new ArrayList<String>();
        sharedLses.addAll(listeCodes1);
        sharedLses.removeAll(addedLses);
        sharedLses.removeAll(missingsLses);
        return Collections.unmodifiableList((List<? extends String>)sharedLses);
    }
    
    private List<String> getListOfLsesCodes(final List<ListeApogee> listeLses) {
        final List<String> listeCodes = new ArrayList<String>();
        for (final ListeApogee lse : listeLses) {
            listeCodes.add(lse.getCodeLse());
        }
        return listeCodes;
    }
    
    public List<String> afficheDiffElp(final ElpApogee elp1, final ElpApogee elp2) {
        final List<String> diffParams = new ArrayList<String>();
        if (elp1 != null && elp2 != null) {
            diffParams.add(this.compareParam(elp1.getCodeElp(), elp2.getCodeElp()));
            diffParams.add(this.compareParam(elp1.getType().toString(), elp2.getType().toString()));
            diffParams.add(this.compareParam(elp1.getNom(), elp2.getNom()));
            diffParams.add(this.compareParam(elp1.getAcronyme(), elp2.getAcronyme()));
            diffParams.add(this.compareNumericParam(elp1.getEcts(), elp2.getEcts()));
            diffParams.addAll(this.compareHeures(elp1, elp2));
        }
        return Collections.unmodifiableList((List<? extends String>)diffParams);
    }
    
    public void getDescendantsElp(final ElpApogee elp, final List<ElpApogee> listeElps, final List<ListeApogee> listeLses) {
        if (elp != null) {
            for (final ListeApogee lse : elp.getListe()) {
                listeLses.add(lse);
                this.getDescendantsListeApogee(lse, listeElps, listeLses);
            }
        }
    }
    
    public List<String> getSharedElps(final List<ElpApogee> listeElps1, final List<ElpApogee> listeElps2, final List<String> missingsElps, final List<String> addedElps) {
        final List<String> listeCodes1 = this.getListOfElpsCodes(listeElps1);
        final List<String> listeCodes2 = this.getListOfElpsCodes(listeElps2);
        missingsElps.addAll(listeCodes2);
        addedElps.addAll(listeCodes1);
        missingsElps.removeAll(listeCodes1);
        addedElps.removeAll(listeCodes2);
        final List<String> sharedElps = new ArrayList<String>(listeCodes1);
        sharedElps.removeAll(addedElps);
        sharedElps.removeAll(missingsElps);
        return Collections.unmodifiableList((List<? extends String>)sharedElps);
    }
    
    private List<String> getListOfElpsCodes(final List<ElpApogee> listeElp) {
        final List<String> listeCodes = new ArrayList<String>();
        for (final ElpApogee elp : listeElp) {
            if (elp != null) {
                listeCodes.add(elp.getCodeElp());
            }
        }
        return listeCodes;
    }
    
    private List<String> compareHeures(final ElpApogee elp1, final ElpApogee elp2) {
        final List<String> diffHeures = new ArrayList<String>();
        final Heure h1 = this.univ1.getHeureBycode(elp1.getCodeElp());
        final Heure h2 = this.univ2.getHeureBycode(elp2.getCodeElp());
        if (h1 != null && h2 != null) {
            diffHeures.add(this.compareNumericParam(h1.getNbreCM(), h2.getNbreCM()));
            diffHeures.add(this.compareNumericParam(h1.getNbreTD(), h2.getNbreTD()));
            diffHeures.add(this.compareNumericParam(h1.getNbreTP(), h2.getNbreTP()));
            diffHeures.add(this.compareNumericParam(h1.getNbreProjet(), h2.getNbreProjet()));
            diffHeures.add(this.compareNumericParam(h1.getNbreProjet(), h2.getNbreProjet()));
            diffHeures.add(this.compareNumericParam(h1.getNbreTdm(), h2.getNbreTdm()));
        }
        else {
            diffHeures.addAll(Arrays.asList("", "", "", "", "", ""));
        }
        return Collections.unmodifiableList((List<? extends String>)diffHeures);
    }
    
    private String compareParam(final String s1, final String s2) {
        final String diff = String.valueOf(s1) + " / " + s2;
        if (!s1.equals(s2)) {
            return diff;
        }
        return s2;
    }
    
    private String compareNumericParam(final String p1, final String p2) {
        float f1 = 0.0f;
        float f2 = 0.0f;
        if (this.isANumber(p1)) {
            f1 = Float.valueOf(p1);
        }
        if (this.isANumber(p2)) {
            f2 = Float.valueOf(p2);
        }
        final String diff = String.valueOf(p1) + " / " + p2;
        if (f1 != f2) {
            return diff;
        }
        return p2;
    }
    
    private boolean isANumber(final String chaine) {
        try {
            Float.parseFloat(chaine);
        }
        catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
