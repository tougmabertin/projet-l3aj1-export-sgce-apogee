// 
// Decompiled by Procyon v0.5.36
// 

package apogee.Heure;

public class Heure
{
    private String codeElp;
    private String nbreCM;
    private String nbreTD;
    private String nbreTp;
    private String nbreProjet;
    private String nbreStage;
    private String nbreTdm;
    
    public Heure(final String codeElp, final String nbreCM, final String nbreTD, final String nbreTp) {
        this.codeElp = codeElp;
        this.nbreCM = nbreCM;
        this.nbreTD = nbreTD;
        this.nbreTp = nbreTp;
    }
    
    public Heure(final String codeElp, final String nbreCM, final String nbreTD, final String nbreTp, final String nbreP, final String nbreS, final String nbreT) {
        this.codeElp = codeElp;
        this.nbreCM = nbreCM;
        this.nbreTD = nbreTD;
        this.nbreTp = nbreTp;
        this.setNbreProjet(nbreP);
        this.setNbreStage(nbreS);
        this.setNbreTdm(nbreT);
    }
    
    public String getCodeElp() {
        return this.codeElp;
    }
    
    public String getNbreCM() {
        return this.nbreCM;
    }
    
    public String getNbreTD() {
        return this.nbreTD;
    }
    
    public String getNbreTP() {
        return this.nbreTp;
    }
    
    @Override
    public String toString() {
        String res = "code elp : " + this.codeElp + "\t CM : " + this.nbreCM + "\t TD : " + this.nbreTD + "\t TP :" + this.nbreTp;
        if (this.nbreProjet != null) {
            res = String.valueOf(res) + "\t Projet : " + this.nbreProjet;
        }
        if (this.nbreStage != null) {
            res = String.valueOf(res) + "\t Stage : " + this.nbreStage;
        }
        if (this.nbreTdm != null) {
            res = String.valueOf(res) + "\t TDM : " + this.nbreTdm;
        }
        return res;
    }
    
    public String getNbreProjet() {
        return this.nbreProjet;
    }
    
    public void setNbreProjet(final String nbreProjet) {
        this.nbreProjet = nbreProjet;
    }
    
    public String getNbreStage() {
        return this.nbreStage;
    }
    
    public void setNbreStage(final String nbreStage) {
        this.nbreStage = nbreStage;
    }
    
    public String getNbreTdm() {
        return this.nbreTdm;
    }
    
    public void setNbreTdm(final String nbreTdm) {
        this.nbreTdm = nbreTdm;
    }
}
