// 
// Decompiled by Procyon v0.5.36
// 

package apogee.universite;

import java.util.LinkedList;
import java.util.Collection;
import java.util.ArrayList;
import apogee.liste.ListeApogee;
import apogee.elp.TypeElp;
import apogee.elp.ElpApogee;
import apogee.Heure.Heure;
import apogee.formation.Formation;
import java.util.LinkedHashMap;

public class UniversiteApogee
{
    private LinkedHashMap<String, Formation> formations;
    private LinkedHashMap<String, Heure> heures;
    private LinkedHashMap<String, ElpApogee> map;
    private LinkedHashMap<TypeElp, LinkedHashMap<String, ElpApogee>> elps;
    private LinkedHashMap<String, ListeApogee> listesApogee;
    
    public UniversiteApogee() {
        this.formations = new LinkedHashMap<String, Formation>();
        this.heures = new LinkedHashMap<String, Heure>();
        this.map = new LinkedHashMap<String, ElpApogee>();
        this.elps = new LinkedHashMap<TypeElp, LinkedHashMap<String, ElpApogee>>();
        this.listesApogee = new LinkedHashMap<String, ListeApogee>();
    }
    
    public void addFormation(final Formation form) {
        if (this.formations.get(form.getCodeEtp()) != null && !this.formations.get(form.getCodeEtp()).getNom().equalsIgnoreCase(form.getNom())) {
            this.formations.put(String.valueOf(form.getCodeEtp()) + "2", form);
        }
        else {
            this.formations.put(form.getCodeEtp(), form);
        }
    }
    
    public Formation getFormationByCode(final String code) {
        return this.formations.get(code);
    }
    
    public ArrayList<Formation> getToutesLesFormations() {
        final ArrayList<Formation> listFormations = new ArrayList<Formation>(this.formations.values());
        return listFormations;
    }
    
    public void addElpApogee(final ElpApogee elp) {
        this.map.put(elp.getCodeElp(), elp);
        this.elps.put(elp.getType(), this.map);
    }
    
    public LinkedList<ElpApogee> getElpByType(final TypeElp type) {
        final LinkedHashMap<String, ElpApogee> map = this.elps.get(type);
        final LinkedList<ElpApogee> liste = new LinkedList<ElpApogee>(map.values());
        return liste;
    }
    
    public ElpApogee getElpByCode(final String code) {
        return this.map.get(code);
    }
    
    public ArrayList<ElpApogee> getToutElps() {
        final ArrayList<ElpApogee> liste = new ArrayList<ElpApogee>(this.map.values());
        return liste;
    }
    
    public void addListeApogee(final ListeApogee lse) {
        this.listesApogee.put(lse.getCodeLse(), lse);
    }
    
    public ListeApogee getListeApogeeByCode(final String code) {
        return this.listesApogee.get(code);
    }
    
    public ArrayList<ListeApogee> getToutListesApogee() {
        final ArrayList<ListeApogee> liste = new ArrayList<ListeApogee>(this.listesApogee.values());
        return liste;
    }
    
    public Heure getHeureBycode(final String code) {
        return this.heures.get(code);
    }
    
    public void addHeure(final Heure heure) {
        this.heures.put(heure.getCodeElp(), heure);
    }
    
    public ArrayList<Heure> getToutHeures() {
        final ArrayList<Heure> listHeure = new ArrayList<Heure>(this.heures.values());
        return listHeure;
    }
}
