// 
// Decompiled by Procyon v0.5.36
// 

package apogee.csvreader;

import apogee.liste.ListeApogee;
import apogee.Heure.Heure;
import apogee.elp.ElpApogee;
import java.util.Iterator;
import apogee.formation.Formation;
import apogee.universite.UniversiteApogee;
import apogee.liste.TypeListe;
import apogee.elp.TypeElp;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.io.IOException;
import java.io.Reader;
import com.opencsv.CSVReader;
import java.io.FileReader;

public class CsvApogeeReader
{
    private int indice_codeElpArb;
    private int indice_typeElpArb;
    private int indice_codePereElpArb;
    private int indice_acronymeElpArb;
    private int indice_nomElpArb;
    private int indice_ectsElpArb;
    private int indice_codeElpVet;
    private int indice_typeElpVet;
    private int indice_acronymeElpVet;
    private int indice_nomElpVet;
    private int indice_ectsElpVet;
    private int indice_codeLse;
    private int indice_typeLse;
    private int indice_nomLse;
    private int indice_nbreChxMin;
    private int indice_nbreChxMax;
    private int indice_version;
    private int indice_codeVet;
    private int indice_acronymeVet;
    private int indice_nomVet;
    private int indice_ectsVet;
    private int indice_codeLseVet;
    private int indice_typeLseVet;
    private int indice_nomLseVet;
    private int indice_nbreChxMinVet;
    private int indice_nbreChxMaxVet;
    private int indice_codeElp;
    private int indice_nbreCM;
    private int indice_nbreTD;
    private int indice_nbreTP;
    private int indice_niveau;
    
    public void premiereLectureFichiers(final String file) throws IOException {
        Throwable t = null;
        try {
            final FileReader reader = new FileReader(file);
            try {
                final CSVReader csvReader = new CSVReader((Reader)reader);
                try {
                    final String[] read = csvReader.readNext();
                    for (int i = 0; i < read.length; ++i) {
                        final String s;
                        switch (s = read[i]) {
                            case "nbr_max_elp_obl_chx": {
                                this.indice_nbreChxMax = i;
                                break;
                            }
                            case "niveau": {
                                this.indice_niveau = i;
                                break;
                            }
                            case "crd_elp_fils": {
                                this.indice_ectsElpArb = i;
                                break;
                            }
                            case "nbr_min_elp_obl_chx_vet": {
                                this.indice_nbreChxMinVet = i;
                                break;
                            }
                            case "typ_lse": {
                                this.indice_typeLseVet = i;
                                break;
                            }
                            case "nbr_max_elp_obl_chx_vet": {
                                this.indice_nbreChxMaxVet = i;
                                break;
                            }
                            case "lic_elp_fils": {
                                this.indice_acronymeElpArb = i;
                                break;
                            }
                            case "lib_elp_fils": {
                                this.indice_nomElpArb = i;
                                break;
                            }
                            case "TYP_LSE": {
                                this.indice_typeLse = i;
                                break;
                            }
                            case "lib_elp": {
                                this.setIndice_nomElpVet(i);
                                break;
                            }
                            case "lib_etp": {
                                this.indice_nomVet = i;
                                break;
                            }
                            case "lic_elp": {
                                this.indice_acronymeElpVet = i;
                                break;
                            }
                            case "lic_etp": {
                                this.indice_acronymeVet = i;
                                break;
                            }
                            case "lic_lse": {
                                this.indice_nomLseVet = i;
                                break;
                            }
                            case "lic_nel": {
                                this.indice_typeElpVet = i;
                                break;
                            }
                            case "cod_elp_fils": {
                                this.indice_codeElpArb = i;
                                break;
                            }
                            case "cod_elp_pere": {
                                this.indice_codePereElpArb = i;
                                break;
                            }
                            case "lic_nel_fils": {
                                this.indice_typeElpArb = i;
                                break;
                            }
                            case "cod_vrs_vet": {
                                this.indice_version = i;
                                break;
                            }
                            case "LIC_LSE": {
                                this.indice_nomLse = i;
                                break;
                            }
                            case "cod_elp": {
                                this.indice_codeElpVet = i;
                                break;
                            }
                            case "cod_etp": {
                                this.indice_codeVet = i;
                                break;
                            }
                            case "cod_lse": {
                                this.indice_codeLseVet = i;
                                break;
                            }
                            case "NBR_HEU_CM_ELP": {
                                this.indice_nbreCM = i;
                                break;
                            }
                            case "nbr_crd_elp": {
                                this.indice_ectsElpVet = i;
                                break;
                            }
                            case "nbr_crd_vet": {
                                this.indice_ectsVet = i;
                                break;
                            }
                            case "nbr_min_elp_obl_chx": {
                                this.indice_nbreChxMin = i;
                                break;
                            }
                            case "COD_ELP": {
                                this.indice_codeElp = i;
                                break;
                            }
                            case "COD_LSE": {
                                this.indice_codeLse = i;
                                break;
                            }
                            case "NBR_HEU_TD_ELP": {
                                this.indice_nbreTD = i;
                                break;
                            }
                            case "NBR_HEU_TP_ELP": {
                                this.indice_nbreTP = i;
                                break;
                            }
                            default:
                                break;
                        }
                    }
                }
                finally {
                    if (csvReader != null) {
                        csvReader.close();
                    }
                }
                if (reader != null) {
                    reader.close();
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception;
                    t = exception;
                }
                else {
                    final Throwable exception;
                    if (t != exception) {
                        t.addSuppressed(exception);
                    }
                }
                if (reader != null) {
                    reader.close();
                }
            }
        }
        finally {
            if (t == null) {
                final Throwable exception2;
                t = exception2;
            }
            else {
                final Throwable exception2;
                if (t != exception2) {
                    t.addSuppressed(exception2);
                }
            }
        }
    }
    
    public static LinkedList<String[]> readFileCSV(final String file) throws FileNotFoundException {
        final LinkedList<String[]> data = new LinkedList<String[]>();
        try {
            Throwable t = null;
            try {
                final FileReader reader = new FileReader(file);
                try {
                    final CSVReader csvReader = new CSVReader((Reader)reader);
                    try {
                        csvReader.readNext();
                        String[] read;
                        while ((read = csvReader.readNext()) != null) {
                            final int size = read.length;
                            if (size == 0) {
                                continue;
                            }
                            data.add(read);
                        }
                    }
                    finally {
                        if (csvReader != null) {
                            csvReader.close();
                        }
                    }
                    if (reader != null) {
                        reader.close();
                        return data;
                    }
                    return data;
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                    if (reader != null) {
                        reader.close();
                    }
                }
            }
            finally {
                if (t == null) {
                    final Throwable exception2;
                    t = exception2;
                }
                else {
                    final Throwable exception2;
                    if (t != exception2) {
                        t.addSuppressed(exception2);
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
    
    public TypeElp getTypeElpFromEnum(final String type) {
        final TypeElp typeElp = type.equals("Parcours") ? TypeElp.PARCOURS : (type.equals("Semestre") ? TypeElp.SEMESTRE : (type.equals("Bloc") ? TypeElp.BLOC : (type.equals("U.E.") ? TypeElp.UE : (type.equals("U.F.") ? TypeElp.UF : (type.equals("ECUE") ? TypeElp.ECUE : (type.equals("Mati\u00e8re") ? TypeElp.MATIERE : (type.equals("Enseignt") ? TypeElp.ENS : (type.equals("Ann\u00e9e") ? TypeElp.ANNEE : TypeElp.NR))))))));
        return typeElp;
    }
    
    public TypeListe getTypeListeFromEnum(final String type) {
        final TypeListe typeListe = type.equals("Obligatoire") ? TypeListe.O : (type.equals("Facultative") ? TypeListe.F : TypeListe.OC);
        return typeListe;
    }
    
    public void putAllFormations(final UniversiteApogee universite, final String source) throws IOException {
        final LinkedList<String[]> data = readFileCSV(source);
        this.premiereLectureFichiers(source);
        for (final String[] read : data) {
            final String versionEtp = read[this.indice_version].trim();
            final String codeEtp = read[this.indice_codeVet].trim();
            final String acronyme = read[this.indice_acronymeVet].trim();
            final String nom = read[this.indice_nomVet].trim();
            final String ects = read[this.indice_ectsVet].trim();
            final Formation form = new Formation(versionEtp, codeEtp, acronyme, nom, ects);
            if (!universite.getToutesLesFormations().contains(form)) {
                universite.addFormation(form);
            }
        }
    }
    
    public void putAllElps(final UniversiteApogee universite, final String elpSource, final String vetSource) throws IOException {
        this.premiereLectureFichiers(elpSource);
        final LinkedList<String[]> data = readFileCSV(elpSource);
        final LinkedList<String[]> data2 = readFileCSV(vetSource);
        for (final String[] read : data) {
            final String codeElp = read[this.indice_codeElpArb].trim();
            final String acronyme = read[this.indice_acronymeElpArb].trim();
            final String nom = read[this.indice_nomElpArb].trim();
            final String ects = read[this.indice_ectsElpArb].trim();
            final TypeElp type = this.getTypeElpFromEnum(read[this.indice_typeElpArb]);
            final ElpApogee elp = new ElpApogee(codeElp, acronyme, nom, type, ects);
            if (universite.getElpByCode(codeElp) == null) {
                universite.addElpApogee(elp);
            }
            elp.setNiveau(Integer.parseInt(read[this.indice_niveau]) + 1);
        }
        this.premiereLectureFichiers(vetSource);
        for (final String[] read : data2) {
            final String codeElp = read[this.indice_codeElpVet].trim();
            final String acronyme = read[this.indice_acronymeElpVet].trim();
            final String nom = read[this.indice_acronymeElpVet].trim();
            final String ects = read[this.indice_ectsElpVet].trim();
            final TypeElp type = this.getTypeElpFromEnum(read[this.indice_typeElpVet]);
            if (type != TypeElp.ANNEE) {
                final ElpApogee elp = new ElpApogee(codeElp, acronyme, nom, type, ects);
                if (universite.getElpByCode(codeElp) == null) {
                    universite.addElpApogee(elp);
                }
                elp.setNiveau(2);
            }
        }
    }
    
    public void putAllHeure(final UniversiteApogee universite, final String source) throws IOException {
        this.premiereLectureFichiers(source);
        final LinkedList<String[]> data = readFileCSV(source);
        for (final String[] read : data) {
            final String codeElp = read[this.indice_codeElp];
            final String nbreCM = read[this.indice_nbreCM];
            final String nbreTD = read[this.indice_nbreTD];
            final String nbreTP = read[this.indice_nbreTP];
            final String nbreProjet = "";
            final String nbreStage = "";
            final String nbreTDM = "";
            final Heure heure = new Heure(codeElp, nbreCM, nbreTD, nbreTP, nbreProjet, nbreStage, nbreTDM);
            universite.addHeure(heure);
        }
    }
    
    public void putAllListesApogee(final UniversiteApogee universite, final String elpSource, final String vetSource) throws IOException {
        this.premiereLectureFichiers(elpSource);
        final LinkedList<String[]> data = readFileCSV(elpSource);
        final LinkedList<String[]> data2 = readFileCSV(vetSource);
        for (final String[] read : data) {
            final String codeLse = read[this.indice_codeLse].trim();
            final TypeListe type = this.getTypeListeFromEnum(read[this.indice_typeLse]);
            final String nom = read[this.indice_nomLse].trim();
            final String nbreChoixMin = read[this.indice_nbreChxMin].trim();
            final String nbreChoixMax = read[this.indice_nbreChxMax].trim();
            final String codeElpfils = read[this.indice_codeElpArb].trim();
            final String codePere = read[this.indice_codePereElpArb].trim();
            final ElpApogee elpFils = universite.getElpByCode(codeElpfils);
            final ElpApogee elpPere = universite.getElpByCode(codePere);
            if (universite.getListeApogeeByCode(codeLse) == null) {
                final ListeApogee liste = new ListeApogee(codeLse, type, nom, nbreChoixMin, nbreChoixMax);
                liste.addListeElp(elpFils);
                universite.addListeApogee(liste);
                if (elpPere == null) {
                    continue;
                }
                elpPere.addListeApogee(liste);
            }
            else {
                universite.getListeApogeeByCode(codeLse).addListeElp(elpFils);
                if (elpPere == null) {
                    continue;
                }
                elpPere.addListeApogee(universite.getListeApogeeByCode(codeLse));
            }
        }
        this.premiereLectureFichiers(vetSource);
        for (final String[] read : data2) {
            final String codeLse = read[this.indice_codeLseVet].trim();
            final TypeListe type = this.getTypeListeFromEnum(read[this.indice_typeLseVet]);
            final String nom = read[this.indice_nomLseVet].trim();
            final String nbreChoixMin = read[this.indice_nbreChxMinVet].trim();
            final String nbreChoixMax = read[this.indice_nbreChxMaxVet].trim();
            final String codeElpfils = read[this.indice_codeElpVet].trim();
            final String codeElpPere = read[this.indice_codeVet].trim();
            final ElpApogee elpFils = universite.getElpByCode(codeElpfils);
            final Formation vet = universite.getFormationByCode(codeElpPere);
            if (universite.getListeApogeeByCode(codeLse) == null) {
                final ListeApogee liste = new ListeApogee(codeLse, type, nom, nbreChoixMin, nbreChoixMax);
                liste.addListeElp(elpFils);
                universite.addListeApogee(liste);
                vet.addListeApogee(liste);
            }
            else {
                universite.getListeApogeeByCode(codeLse).addListeElp(elpFils);
                vet.addListeApogee(universite.getListeApogeeByCode(codeLse));
            }
        }
    }
    
    public int getIndice_nomElpVet() {
        return this.indice_nomElpVet;
    }
    
    public void setIndice_nomElpVet(final int indice_nomElpVet) {
        this.indice_nomElpVet = indice_nomElpVet;
    }
}
