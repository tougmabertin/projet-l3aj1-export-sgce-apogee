// 
// Decompiled by Procyon v0.5.36
// 

package apogee.formation;

import java.util.Iterator;
import java.util.Collections;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import apogee.liste.ListeApogee;
import java.util.List;

public class Formation
{
    private String versionEtp;
    private String codeEtp;
    private String acronyme;
    private String nom;
    private String ects;
    private List<ListeApogee> liste;
    Map<String, ListeApogee> ls;
    
    public Formation(final String versionEtp, final String codeEtp, final String acronyme, final String nom, final String ects) {
        this.liste = new ArrayList<ListeApogee>();
        this.ls = new HashMap<String, ListeApogee>();
        this.versionEtp = versionEtp;
        this.codeEtp = codeEtp;
        this.acronyme = acronyme;
        this.nom = nom;
        this.ects = ects;
    }
    
    public Formation(final String versionEtp, final String codeEtp, final String acronyme, final String nom) {
        this.liste = new ArrayList<ListeApogee>();
        this.ls = new HashMap<String, ListeApogee>();
        this.versionEtp = versionEtp;
        this.codeEtp = codeEtp;
        this.acronyme = acronyme;
        this.nom = nom;
    }
    
    public String getVersion() {
        return this.versionEtp;
    }
    
    public String getCodeEtp() {
        return this.codeEtp;
    }
    
    public String getAcronyme() {
        return this.acronyme;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getEcts() {
        return this.ects;
    }
    
    public List<ListeApogee> getListe() {
        return Collections.unmodifiableList((List<? extends ListeApogee>)this.liste);
    }
    
    public void addListeApogee(final ListeApogee lse) {
        if (!this.liste.contains(lse)) {
            this.liste.add(lse);
        }
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.codeEtp) + "-" + this.versionEtp + "\t" + this.nom;
    }
    
    public void printArborescence() {
        System.out.println(String.valueOf(this.toString()) + "\n");
        for (final ListeApogee lse : this.liste) {
            lse.printArborescence();
        }
    }
}
