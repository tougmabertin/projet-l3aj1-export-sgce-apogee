// 
// Decompiled by Procyon v0.5.36
// 

package apogee.liste;

import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import apogee.elp.ElpApogee;
import java.util.List;

public class ListeApogee
{
    private List<ElpApogee> liste;
    private String codeLse;
    private String nom;
    private String nbreChoixMin;
    private String nbreChoixMax;
    private String codePere;
    private TypeListe type;
    
    public ListeApogee(final String codeLse, final TypeListe type, final String nom, final String nbreChoixMin, final String nbreChoixMax) {
        this.liste = new ArrayList<ElpApogee>();
        this.codeLse = codeLse;
        this.type = type;
        this.nom = nom;
        this.nbreChoixMin = nbreChoixMin;
        this.nbreChoixMax = nbreChoixMin;
    }
    
    public ListeApogee(final String codeLse, final TypeListe type, final String nom) {
        this.liste = new ArrayList<ElpApogee>();
        this.codeLse = codeLse;
        this.type = type;
        this.nom = nom;
    }
    
    public String getCodeLse() {
        return this.codeLse;
    }
    
    public String getNbreChoixMin() {
        return this.nbreChoixMin;
    }
    
    public String getNbreChoixMax() {
        return this.nbreChoixMax;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public TypeListe getType() {
        return this.type;
    }
    
    public void setCodePere(final String codeP) {
        this.codePere = codeP;
    }
    
    public String getCodePere() {
        return this.codePere;
    }
    
    public void addListeElp(final ElpApogee elp) {
        if (!this.liste.contains(elp)) {
            this.liste.add(elp);
        }
    }
    
    public List<ElpApogee> getListe() {
        return Collections.unmodifiableList((List<? extends ElpApogee>)this.liste);
    }
    
    @Override
    public String toString() {
        return this.type + "******LSE*****   " + this.nom + "   " + this.codeLse;
    }
    
    public void printArborescence() {
        System.out.print(" \n" + this.toString() + "\n");
        for (final ElpApogee elp : this.liste) {
            if (elp != null) {
                elp.printArborescence();
            }
        }
    }
}
