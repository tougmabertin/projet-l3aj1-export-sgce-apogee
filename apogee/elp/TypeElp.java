// 
// Decompiled by Procyon v0.5.36
// 

package apogee.elp;

public enum TypeElp
{
    PARCOURS("PARCOURS", 0), 
    SEMESTRE("SEMESTRE", 1), 
    BLOC("BLOC", 2), 
    UE("UE", 3), 
    UF("UF", 4), 
    ECUE("ECUE", 5), 
    MATIERE("MATIERE", 6), 
    ENS("ENS", 7), 
    ANNEE("ANNEE", 8), 
    NR("NR", 9);
    
    private TypeElp(final String name, final int ordinal) {
    }
}
