// 
// Decompiled by Procyon v0.5.36
// 

package apogee.elp;

import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import apogee.Heure.Heure;
import apogee.liste.ListeApogee;
import java.util.List;

public class ElpApogee
{
    private List<ListeApogee> liste;
    private TypeElp type;
    private String codePere;
    private String codeElp;
    private String acronyme;
    private String nom;
    private String ects;
    private int niveau;
    private Heure heures;
    
    public ElpApogee(final String codeElp, final String acronyme, final String nom, final TypeElp type, final String ects) {
        this.liste = new ArrayList<ListeApogee>();
        this.codeElp = codeElp;
        this.acronyme = acronyme;
        this.nom = nom;
        this.ects = ects;
        this.type = type;
    }
    
    public ElpApogee(final String codeElp, final String acronyme, final String nom, final TypeElp type) {
        this.liste = new ArrayList<ListeApogee>();
        this.codeElp = codeElp;
        this.acronyme = acronyme;
        this.nom = nom;
        this.type = type;
    }
    
    public ElpApogee(final String codeElp, final String nom, final TypeElp type) {
        this.liste = new ArrayList<ListeApogee>();
        this.codeElp = codeElp;
        if (nom.length() >= 5) {
            this.acronyme = String.valueOf(nom.substring(0, 3)) + nom.substring(nom.length() - 3, nom.length() - 1);
        }
        else {
            this.acronyme = nom.substring(0);
        }
        this.nom = nom;
        this.type = type;
    }
    
    public ElpApogee(final String codeElp, final String nom, final TypeElp type, final String ects) {
        this.liste = new ArrayList<ListeApogee>();
        this.codeElp = codeElp;
        this.nom = nom;
        if (nom.length() >= 5) {
            this.acronyme = String.valueOf(nom.substring(0, 3)) + nom.substring(nom.length() - 3, nom.length() - 1);
        }
        else {
            this.acronyme = nom.substring(0);
        }
        this.ects = ects;
        this.type = type;
    }
    
    public ElpApogee(final String codeElp, final String acronyme, final String nom, final TypeElp type, final String ects, final Heure heures) {
        this.liste = new ArrayList<ListeApogee>();
        this.codeElp = codeElp;
        this.nom = nom;
        this.acronyme = acronyme;
        this.ects = ects;
        this.type = type;
        this.setHeures(heures);
    }
    
    public String getCodeElp() {
        return this.codeElp;
    }
    
    public String getAcronyme() {
        return this.acronyme;
    }
    
    public String getNom() {
        return this.nom;
    }
    
    public String getEcts() {
        return this.ects;
    }
    
    public TypeElp getType() {
        return this.type;
    }
    
    public String getCodePere() {
        return this.codePere;
    }
    
    public void setCodePere(final String codeP) {
        this.codePere = codeP;
    }
    
    public void setNiveau(final int niveau) {
        this.niveau = niveau;
    }
    
    public int getNiveau() {
        return this.niveau;
    }
    
    public List<ListeApogee> getListe() {
        return Collections.unmodifiableList((List<? extends ListeApogee>)this.liste);
    }
    
    public void addListeApogee(final ListeApogee lse) {
        if (!this.liste.contains(lse)) {
            this.liste.add(lse);
        }
    }
    
    @Override
    public String toString() {
        String res = "   " + this.codeElp + "\t" + this.getNom() + "\t" + this.getEcts();
        if (this.heures != null) {
            res = String.valueOf(res) + "\t" + this.heures.toString();
        }
        return res;
    }
    
    public void printArborescence() {
        System.out.println("  \n" + this.toString() + "\n");
        for (final ListeApogee lse : this.liste) {
            lse.printArborescence();
        }
    }
    
    public Heure getHeures() {
        return this.heures;
    }
    
    public void setHeures(final Heure heures) {
        this.heures = heures;
    }
}
