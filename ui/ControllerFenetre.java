// 
// Decompiled by Procyon v0.5.36
// 

package ui;

import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.event.Event;
import java.util.Collection;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.application.Platform;
import javafx.scene.image.Image;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import sgce.bd.Bd;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import javafx.stage.FileChooser;
import pdf.CreatePdf;
import sgce.elps.Parcours;
import sgce.elps.Mention;
import sgce.elps.Departement;
import java.util.Optional;
import java.io.IOException;
import java.awt.Desktop;
import javafx.scene.control.ButtonType;
import pdf.ComparaisonPdf;
import transformation.SgceToApogee;
import java.util.Iterator;
import apogee.formation.Formation;
import apogee.csvreader.CsvApogeeReader;
import apogee.universite.UniversiteApogee;
import javafx.scene.control.Alert;
import java.io.FilenameFilter;
import javafx.stage.Window;
import javafx.stage.DirectoryChooser;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import java.io.File;
import sgce.universite.UniversiteSgce;
import sgce.bd.Dao;
import javafx.collections.ObservableList;

public class ControllerFenetre
{
    private FenetrePrincipale fenetre;
    private ObservableList<InformationsFichier> listeFichiers;
    private Dao dao;
    private UniversiteSgce universite;
    private File fichierConfig;
    @FXML
    private ImageView ivLogoDescartes;
    @FXML
    private Tab tabReglageBaseSGCE;
    @FXML
    private Button btnSauvegarderBase;
    @FXML
    private ComboBox<String> cbbTypeBase;
    @FXML
    private TextField tfUrlBase;
    @FXML
    private TextField tfUserBase;
    @FXML
    private PasswordField tfMdpBase;
    @FXML
    private Button btnConnexionBDD;
    @FXML
    private TextField tfFichierConfig;
    @FXML
    private Button btnChoixFichierConfig;
    @FXML
    private Tab tabTraitementSGCE;
    @FXML
    private ComboBox<String> cbbChoixUFRSGCE;
    @FXML
    private ComboBox<String> cbbChoixFormationSGCE;
    @FXML
    private ComboBox<String> cbbChoixParcoursSGCE;
    @FXML
    private TextField tfDossierSortieSGCE;
    @FXML
    private Button btnChoixDossierSortieSGCE;
    @FXML
    private TableView<InformationsFichier> tableauFichiers;
    @FXML
    private TableColumn<InformationsFichier, String> colonne1;
    @FXML
    private TableColumn<InformationsFichier, String> colonne2;
    @FXML
    private TableColumn<InformationsFichier, String> colonne3;
    @FXML
    private Button btnTraitementSGCE;
    @FXML
    private Tab tabVerificationApogee;
    @FXML
    private TextField tfChoixDossierApogee;
    @FXML
    private Button btnParcourirDossierApogee;
    @FXML
    private ComboBox<String> cbbChoixUFRApogee;
    @FXML
    private ComboBox<String> cbbChoixFormationApogee;
    @FXML
    private ComboBox<String> cbbChoixFormationComparaisonApogee;
    @FXML
    private TextField tfDossierSortieApogee;
    @FXML
    private Button btnParcourirDossierSortieApogee;
    @FXML
    private TextField tfNomFichierSortieApogee;
    @FXML
    private Button btnTraitementApogee;
    
    @FXML
    void choixDossierSortieSGCE(final ActionEvent event) {
        this.fenetre.setDisable(true);
        final DirectoryChooser rChooser = new DirectoryChooser();
        rChooser.setTitle("Choisir un r\u00e9pertoire");
        final File dossierChoisi = rChooser.showDialog((Window)null);
        if (dossierChoisi != null) {
            this.tfDossierSortieSGCE.setText(dossierChoisi.getAbsolutePath());
        }
        this.fenetre.setDisable(false);
    }
    
    @FXML
    void parcourirDossierApogee(final ActionEvent event) {
        this.fenetre.setDisable(true);
        final DirectoryChooser rChooser = new DirectoryChooser();
        rChooser.setTitle("Choisir un r\u00e9pertoire");
        final File dossierChoisi = rChooser.showDialog((Window)null);
        if (dossierChoisi != null) {
            this.tfChoixDossierApogee.setText(dossierChoisi.getAbsolutePath());
            this.cbbChoixFormationComparaisonApogee.setDisable(false);
            final File[] fichiersCsv = dossierChoisi.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(final File dir, final String name) {
                    return name.toLowerCase().endsWith(".csv");
                }
            });
            if (fichiersCsv.length < 3) {
                final Alert alerte = new Alert(Alert.AlertType.ERROR);
                alerte.setTitle("Dossier incomplet");
                alerte.setHeaderText((String)null);
                alerte.setContentText("Le dossier doit contenir au minimum les fichiers \"VET\", \"Arborescence\" et \"Heure\"");
                alerte.showAndWait();
            }
            else {
                File fichierVet = null;
                File fichierElp = null;
                File fichierHeure = null;
                File[] array;
                for (int length = (array = fichiersCsv).length, i = 0; i < length; ++i) {
                    final File f = array[i];
                    if (f.getName().toLowerCase().contains("vet")) {
                        fichierVet = f;
                    }
                    if (f.getName().toLowerCase().contains("arborescence") || f.getName().toLowerCase().contains("elp")) {
                        fichierElp = f;
                    }
                    if (f.getName().toLowerCase().contains("heure")) {
                        fichierHeure = f;
                    }
                }
                if (fichierVet == null) {
                    final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                    alerte2.setTitle("Fichier VET introuvable");
                    alerte2.setHeaderText((String)null);
                    alerte2.setContentText("Le fichier \"VET\" est introuvable ou mal nomm\u00e9");
                    alerte2.showAndWait();
                    return;
                }
                if (fichierElp == null) {
                    final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                    alerte2.setTitle("Fichier Arborescence introuvable");
                    alerte2.setHeaderText((String)null);
                    alerte2.setContentText("Le fichier \"Arborescence\" est introuvable ou mal nomm\u00e9");
                    alerte2.showAndWait();
                    return;
                }
                if (fichierHeure == null) {
                    final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                    alerte2.setTitle("Fichier Heures introuvable");
                    alerte2.setHeaderText((String)null);
                    alerte2.setContentText("Le fichier \"Heures\" est introuvable ou mal nomm\u00e9");
                    alerte2.showAndWait();
                    return;
                }
                final UniversiteApogee univA = new UniversiteApogee();
                final CsvApogeeReader reader = new CsvApogeeReader();
                try {
                    reader.putAllHeure(univA, fichierHeure.getAbsolutePath());
                    reader.putAllElps(univA, fichierElp.getAbsolutePath(), fichierVet.getAbsolutePath());
                    reader.putAllFormations(univA, fichierVet.getAbsolutePath());
                    reader.putAllListesApogee(univA, fichierElp.getAbsolutePath(), fichierVet.getAbsolutePath());
                }
                catch (Exception e) {
                    final Alert alerte3 = new Alert(Alert.AlertType.ERROR);
                    alerte3.setTitle("Erreur de lecture");
                    alerte3.setHeaderText((String)null);
                    alerte3.setContentText("Erreur lors de la lecture des fichiers \".csv\"");
                    alerte3.showAndWait();
                    return;
                }
                for (final Formation f2 : univA.getToutesLesFormations()) {
                    this.cbbChoixFormationComparaisonApogee.getItems().add((Object)f2.getNom());
                }
            }
        }
        this.fenetre.setDisable(false);
    }
    
    @FXML
    void parcourirDossierSortieApogee(final ActionEvent event) {
        this.fenetre.setDisable(true);
        final DirectoryChooser rChooser = new DirectoryChooser();
        rChooser.setTitle("Choisir un r\u00e9pertoire");
        final File dossierChoisi = rChooser.showDialog((Window)null);
        if (dossierChoisi != null) {
            this.tfDossierSortieApogee.setText(dossierChoisi.getAbsolutePath());
        }
        this.fenetre.setDisable(false);
    }
    
    @FXML
    void traitementApogee(final ActionEvent event) {
        final File repertoireCSV = new File(this.tfChoixDossierApogee.getText());
        if (this.cbbChoixFormationApogee.getSelectionModel().getSelectedIndex() == -1) {
            final Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setTitle("Choix de la Formation");
            alerte.setHeaderText((String)null);
            alerte.setContentText("Veuillez choisir une Formation");
            alerte.showAndWait();
            return;
        }
        if (this.cbbChoixFormationComparaisonApogee.getSelectionModel().getSelectedIndex() == -1) {
            final Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setTitle("Choix de la Formation");
            alerte.setHeaderText((String)null);
            alerte.setContentText("Veuillez choisir une Formation");
            alerte.showAndWait();
            return;
        }
        if (!repertoireCSV.exists()) {
            final Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setTitle("Dossier introuvable");
            alerte.setHeaderText((String)null);
            alerte.setContentText("Le dossier contenant les fichiers \".csv\" que vous avez mentionn\u00e9 n'existe pas");
            alerte.showAndWait();
        }
        else {
            final File[] fichiersCsv = repertoireCSV.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(final File dir, final String name) {
                    return name.toLowerCase().endsWith(".csv");
                }
            });
            if (fichiersCsv.length < 3) {
                final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                alerte2.setTitle("Dossier incomplet");
                alerte2.setHeaderText((String)null);
                alerte2.setContentText("Le dossier doit contenir au minimum les fichiers \"VET\", \"Arborescence\" et \"Heure\"");
                alerte2.showAndWait();
            }
            else {
                File fichierVet = null;
                File fichierElp = null;
                File fichierHeure = null;
                File[] array;
                for (int length = (array = fichiersCsv).length, i = 0; i < length; ++i) {
                    final File f = array[i];
                    if (f.getName().toLowerCase().contains("vet")) {
                        fichierVet = f;
                    }
                    if (f.getName().toLowerCase().contains("arborescence") || f.getName().toLowerCase().contains("elp")) {
                        fichierElp = f;
                    }
                    if (f.getName().toLowerCase().contains("heure")) {
                        fichierHeure = f;
                    }
                }
                if (fichierVet == null) {
                    final Alert alerte3 = new Alert(Alert.AlertType.ERROR);
                    alerte3.setTitle("Fichier VET introuvable");
                    alerte3.setHeaderText((String)null);
                    alerte3.setContentText("Le fichier \"VET\" est introuvable ou mal nomm\u00e9");
                    alerte3.showAndWait();
                    return;
                }
                if (fichierElp == null) {
                    final Alert alerte3 = new Alert(Alert.AlertType.ERROR);
                    alerte3.setTitle("Fichier Arborescence introuvable");
                    alerte3.setHeaderText((String)null);
                    alerte3.setContentText("Le fichier \"Arborescence\" est introuvable ou mal nomm\u00e9");
                    alerte3.showAndWait();
                    return;
                }
                if (fichierHeure == null) {
                    final Alert alerte3 = new Alert(Alert.AlertType.ERROR);
                    alerte3.setTitle("Fichier Heures introuvable");
                    alerte3.setHeaderText((String)null);
                    alerte3.setContentText("Le fichier \"Heures\" est introuvable ou mal nomm\u00e9");
                    alerte3.showAndWait();
                    return;
                }
                final UniversiteApogee univA = new UniversiteApogee();
                final CsvApogeeReader reader = new CsvApogeeReader();
                try {
                    reader.putAllHeure(univA, fichierHeure.getAbsolutePath());
                    reader.putAllElps(univA, fichierElp.getAbsolutePath(), fichierVet.getAbsolutePath());
                    reader.putAllFormations(univA, fichierVet.getAbsolutePath());
                    reader.putAllListesApogee(univA, fichierElp.getAbsolutePath(), fichierVet.getAbsolutePath());
                }
                catch (Exception e) {
                    final Alert alerte4 = new Alert(Alert.AlertType.ERROR);
                    alerte4.setTitle("Erreur lors de la lecture des fichiers");
                    alerte4.setHeaderText((String)null);
                    alerte4.setContentText("Un probl\u00e8me est survenu lors de la lecture des fichiers \".csv\"");
                    alerte4.showAndWait();
                    e.printStackTrace();
                    return;
                }
                if (this.tfDossierSortieApogee.getText().equals("") || !new File(this.tfDossierSortieApogee.getText()).exists()) {
                    final Alert alerte5 = new Alert(Alert.AlertType.ERROR);
                    alerte5.setTitle("Dossier introuvable");
                    alerte5.setHeaderText((String)null);
                    alerte5.setContentText("Le dossier choisi pour la cr\u00e9ation du fichier \".pdf\" est introuvable");
                    alerte5.showAndWait();
                    return;
                }
                UniversiteApogee univS = new UniversiteApogee();
                final SgceToApogee transformation = new SgceToApogee(univS);
                univS = transformation.transformerArborescence(this.universite);
                String nomFichier = "";
                if (this.tfNomFichierSortieApogee.getText().trim().length() == 0) {
                    nomFichier = String.valueOf(this.cbbChoixFormationApogee.getSelectionModel().getSelectedItem()) + " - V\u00e9rification.pdf";
                }
                else {
                    nomFichier = String.valueOf(this.tfNomFichierSortieApogee.getText()) + " - V\u00e9rification.pdf";
                }
                final ComparaisonPdf creationPdf = new ComparaisonPdf(String.valueOf(this.tfDossierSortieApogee.getText()) + "/" + nomFichier, univA, univS);
                Formation formationS = null;
                for (final Formation f2 : univS.getToutesLesFormations()) {
                    if (f2.getNom().equalsIgnoreCase((String)this.cbbChoixFormationApogee.getSelectionModel().getSelectedItem())) {
                        formationS = f2;
                    }
                }
                if (formationS == null) {
                    final Alert alerte6 = new Alert(Alert.AlertType.ERROR);
                    alerte6.setTitle("Erreur de comparaison");
                    alerte6.setHeaderText((String)null);
                    alerte6.setContentText("Erreur lors de la comparaison : Formation introuvable");
                    alerte6.showAndWait();
                    return;
                }
                Formation formationA = null;
                for (final Formation f3 : univA.getToutesLesFormations()) {
                    if (f3.getNom().equalsIgnoreCase((String)this.cbbChoixFormationComparaisonApogee.getSelectionModel().getSelectedItem())) {
                        formationA = f3;
                    }
                }
                if (formationA == null) {
                    final Alert alerte7 = new Alert(Alert.AlertType.ERROR);
                    alerte7.setTitle("Erreur de comparaison");
                    alerte7.setHeaderText((String)null);
                    alerte7.setContentText("Erreur lors de la comparaison : Formation introuvable");
                    alerte7.showAndWait();
                    return;
                }
                try {
                    creationPdf.createPdf(formationA, formationS);
                    final Alert alerte7 = new Alert(Alert.AlertType.INFORMATION);
                    alerte7.setTitle("Traitement termin\u00e9");
                    alerte7.setHeaderText((String)null);
                    alerte7.setContentText("Le traitement s'est correctement termin\u00e9 !\nSouhaitez-vous ouvrir le dossier contenant les fichiers \".pdf\" ?");
                    final ButtonType btnOui = new ButtonType("Oui");
                    final ButtonType btnNon = new ButtonType("Non");
                    alerte7.getButtonTypes().setAll((Object[])new ButtonType[] { btnOui, btnNon });
                    final Optional<ButtonType> btnAppuye = (Optional<ButtonType>)alerte7.showAndWait();
                    if (btnAppuye.get() == btnOui) {
                        try {
                            Desktop.getDesktop().open(new File(this.tfDossierSortieApogee.getText()));
                        }
                        catch (IOException e3) {
                            final Alert alerte8 = new Alert(Alert.AlertType.ERROR);
                            alerte8.setTitle("Impossible");
                            alerte8.setHeaderText((String)null);
                            alerte8.setContentText("Erreur lors de l'ouverture du dossier.");
                        }
                        alerte7.close();
                    }
                    else if (btnAppuye.get() == btnNon) {
                        alerte7.close();
                    }
                    this.cbbChoixUFRApogee.getSelectionModel().select(-1);
                    this.cbbChoixFormationApogee.getSelectionModel().select(-1);
                    this.cbbChoixFormationApogee.setDisable(true);
                    this.cbbChoixFormationComparaisonApogee.getSelectionModel().select(-1);
                    this.tfNomFichierSortieApogee.setText("");
                }
                catch (IOException e2) {
                    final Alert alerte9 = new Alert(Alert.AlertType.ERROR);
                    alerte9.setTitle("Erreur d'\u00e9criture");
                    alerte9.setHeaderText((String)null);
                    alerte9.setContentText("Erreur lors de la cr\u00e9ation du fichier \".pdf\" de v\u00e9rification");
                    alerte9.showAndWait();
                    e2.printStackTrace();
                }
            }
        }
    }
    
    @FXML
    void traitementSGCE(final ActionEvent event) {
        boolean traitementReussi = true;
        if (this.universite != null) {
            if (this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1) {
                if (this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1) {
                    if (this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() != -1) {
                        Departement departement = null;
                        sgce.elps.Formation formation = null;
                        Parcours parcours = null;
                        for (final Departement dep : this.universite.getTousLesDepartements()) {
                            if (dep.getNom().equals(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedItem())) {
                                departement = dep;
                                for (final Mention m : dep.getListeMention()) {
                                    for (int i = 0; i < m.getListeFormation().size(); ++i) {
                                        if (m.getListeFormation().get(i).getNom().equals(this.cbbChoixFormationSGCE.getSelectionModel().getSelectedItem())) {
                                            formation = m.getListeFormation().get(i);
                                            for (final Parcours p : formation.getListeParcours()) {
                                                if (p.getNom().equals(this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedItem())) {
                                                    parcours = p;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        final File repertoireSortie = new File(this.tfDossierSortieSGCE.getText());
                        if (!repertoireSortie.exists()) {
                            repertoireSortie.mkdirs();
                        }
                        try {
                            String nomFichier = "";
                            for (final InformationsFichier info : this.tableauFichiers.getItems()) {
                                if (info.getColonne1().equals(this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedItem())) {
                                    nomFichier = info.getColonne3();
                                    if (nomFichier.endsWith(".pdf")) {
                                        continue;
                                    }
                                    nomFichier = String.valueOf(nomFichier) + ".pdf";
                                }
                            }
                            final CreatePdf pdfResultat = new CreatePdf(String.valueOf(repertoireSortie.getAbsolutePath()) + "/" + nomFichier);
                            pdfResultat.createParcoursPdf(departement, formation, parcours, this.universite);
                        }
                        catch (IOException e) {
                            final Alert alerte = new Alert(Alert.AlertType.ERROR);
                            alerte.setTitle("Erreur lors de l'ex\u00e9cution");
                            alerte.setHeaderText((String)null);
                            alerte.setContentText("Impossible de cr\u00e9er le fichier \".pdf\"");
                            alerte.showAndWait();
                            e.printStackTrace();
                            traitementReussi = false;
                        }
                    }
                    else {
                        Departement departement = null;
                        sgce.elps.Formation formation = null;
                        for (final Departement dep2 : this.universite.getTousLesDepartements()) {
                            if (dep2.getNom().equals(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedItem())) {
                                departement = dep2;
                                for (final Mention j : departement.getListeMention()) {
                                    for (final sgce.elps.Formation f : j.getListeFormation()) {
                                        if (f.getNom().equals(this.cbbChoixFormationSGCE.getSelectionModel().getSelectedItem())) {
                                            formation = f;
                                        }
                                    }
                                }
                            }
                        }
                        final File repertoireSortie2 = new File(this.tfDossierSortieSGCE.getText());
                        if (!repertoireSortie2.exists()) {
                            repertoireSortie2.mkdirs();
                        }
                        try {
                            String nomFichier2 = "";
                            for (final InformationsFichier info2 : this.tableauFichiers.getItems()) {
                                if (info2.getColonne1().equals(formation.getNom())) {
                                    nomFichier2 = info2.getColonne3();
                                    if (nomFichier2.endsWith(".pdf")) {
                                        continue;
                                    }
                                    nomFichier2 = String.valueOf(nomFichier2) + ".pdf";
                                }
                            }
                            final CreatePdf pdfResultat2 = new CreatePdf(String.valueOf(repertoireSortie2.getAbsolutePath()) + "/" + nomFichier2);
                            pdfResultat2.createFormationPdf(departement, formation, this.universite);
                        }
                        catch (IOException e2) {
                            final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                            alerte2.setTitle("Erreur lors de l'ex\u00e9cution");
                            alerte2.setHeaderText((String)null);
                            alerte2.setContentText("Impossible de cr\u00e9er le fichier \".pdf\"");
                            alerte2.showAndWait();
                            e2.printStackTrace();
                            traitementReussi = false;
                        }
                    }
                }
                else {
                    Departement departement = null;
                    for (final Departement dep3 : this.universite.getTousLesDepartements()) {
                        if (dep3.getNom().equals(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedItem())) {
                            departement = dep3;
                        }
                    }
                    final File repertoireSortie3 = new File(this.tfDossierSortieSGCE.getText());
                    if (!repertoireSortie3.exists()) {
                        repertoireSortie3.mkdirs();
                    }
                    try {
                        String nomFichier3 = "";
                        for (final InformationsFichier info3 : this.tableauFichiers.getItems()) {
                            if (info3.getColonne1().equals(departement.getNom())) {
                                nomFichier3 = info3.getColonne3();
                                if (nomFichier3.endsWith(".pdf")) {
                                    continue;
                                }
                                nomFichier3 = String.valueOf(nomFichier3) + ".pdf";
                            }
                        }
                        final CreatePdf pdfResultat3 = new CreatePdf(String.valueOf(repertoireSortie3.getAbsolutePath()) + "/" + nomFichier3);
                        pdfResultat3.createDeptPdf(departement, this.universite);
                    }
                    catch (IOException e3) {
                        final Alert alerte3 = new Alert(Alert.AlertType.ERROR);
                        alerte3.setTitle("Erreur lors de l'ex\u00e9cution");
                        alerte3.setHeaderText((String)null);
                        alerte3.setContentText("Impossible de cr\u00e9er le fichier \".pdf\"");
                        alerte3.showAndWait();
                        e3.printStackTrace();
                        traitementReussi = false;
                    }
                }
            }
            else {
                for (final Departement departement : this.universite.getTousLesDepartements()) {
                    final File repertoireSortie2 = new File(this.tfDossierSortieSGCE.getText());
                    if (!repertoireSortie2.exists()) {
                        repertoireSortie2.mkdirs();
                    }
                    try {
                        String nomFichier2 = null;
                        for (final InformationsFichier f2 : this.tableauFichiers.getItems()) {
                            if (f2.getColonne1().equals(departement.getNom())) {
                                nomFichier2 = f2.getColonne3();
                                if (nomFichier2.endsWith(".pdf")) {
                                    continue;
                                }
                                nomFichier2 = String.valueOf(nomFichier2) + ".pdf";
                            }
                        }
                        final CreatePdf pdfResultat2 = new CreatePdf(String.valueOf(repertoireSortie2.getAbsolutePath()) + "/" + nomFichier2);
                        pdfResultat2.createDeptPdf(departement, this.universite);
                    }
                    catch (IOException e2) {
                        final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
                        alerte2.setTitle("Erreur lors de l'ex\u00e9cution");
                        alerte2.setHeaderText((String)null);
                        alerte2.setContentText("Impossible de cr\u00e9er le fichier \".pdf\"");
                        alerte2.showAndWait();
                        e2.printStackTrace();
                        traitementReussi = false;
                    }
                }
            }
            if (traitementReussi) {
                final Alert alerte4 = new Alert(Alert.AlertType.INFORMATION);
                alerte4.setTitle("Traitement termin\u00e9");
                alerte4.setHeaderText((String)null);
                alerte4.setContentText("Le traitement s'est correctement termin\u00e9 !\nSouhaitez-vous ouvrir le dossier contenant les fichiers \".pdf\" ?");
                final ButtonType btnOui = new ButtonType("Oui");
                final ButtonType btnNon = new ButtonType("Non");
                alerte4.getButtonTypes().setAll((Object[])new ButtonType[] { btnOui, btnNon });
                final Optional<ButtonType> btnAppuye = (Optional<ButtonType>)alerte4.showAndWait();
                if (btnAppuye.get() == btnOui) {
                    try {
                        Desktop.getDesktop().open(new File(this.tfDossierSortieSGCE.getText()));
                    }
                    catch (IOException e) {
                        final Alert alerte5 = new Alert(Alert.AlertType.ERROR);
                        alerte5.setTitle("Impossible");
                        alerte5.setHeaderText((String)null);
                        alerte5.setContentText("Erreur lors de l'ouverture du dossier.");
                    }
                    alerte4.close();
                }
                else if (btnAppuye.get() == btnNon) {
                    alerte4.close();
                }
            }
            else {
                final Alert alerte4 = new Alert(Alert.AlertType.ERROR);
                alerte4.setTitle("Erreur de traitement");
                alerte4.setHeaderText((String)null);
                alerte4.setContentText("Une erreur est survenue lors du traitement. Souhaitez-vous r\u00e9essayer ?");
                final ButtonType btnOui = new ButtonType("Oui");
                final ButtonType btnNon = new ButtonType("Non");
                alerte4.getButtonTypes().setAll((Object[])new ButtonType[] { btnOui, btnNon });
                final Optional<ButtonType> btnAppuye = (Optional<ButtonType>)alerte4.showAndWait();
                if (btnAppuye.get() == btnOui) {
                    this.traitementSGCE(null);
                }
                else if (btnAppuye.get() == btnNon) {
                    alerte4.close();
                }
            }
        }
        else {
            final Alert alerte4 = new Alert(Alert.AlertType.ERROR);
            alerte4.setTitle("Traitement impossible");
            alerte4.setHeaderText((String)null);
            alerte4.setContentText("Impossible de lancer le traitement vous n'\u00eates pas connect\u00e9 \u00e0 la base de donn\u00e9es SGCE.");
            alerte4.showAndWait();
        }
    }
    
    @FXML
    void choixFichierConfig(final ActionEvent event) {
        this.fenetre.setDisable(true);
        final FileChooser fChooser = new FileChooser();
        fChooser.setTitle("Ouvrir un fichier Configuration");
        fChooser.getExtensionFilters().add((Object)new FileChooser.ExtensionFilter("Fichier Configuration", new String[] { "*.properties" }));
        fChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Fichier Configuration", new String[] { "*.properties" }));
        final File fichierChoisi = fChooser.showOpenDialog((Window)null);
        if (fichierChoisi != null) {
            this.tfFichierConfig.setText(fichierChoisi.getAbsolutePath());
        }
        this.fenetre.setDisable(false);
        if (fichierChoisi != null) {
            this.fichierConfig = fichierChoisi;
            final Properties prop = new Properties();
            try {
                Throwable t = null;
                try {
                    final FileInputStream input = new FileInputStream(this.fichierConfig);
                    try {
                        prop.load(input);
                        if (prop.getProperty("driver").equalsIgnoreCase("com.mysql.jdbc.Driver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"MySQL");
                        }
                        else if (prop.getProperty("driver").equalsIgnoreCase("com.microsoft.sqlserver.jdbc.SQLServerDriver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"SQL Server");
                        }
                        else if (prop.getProperty("driver").equalsIgnoreCase("oracle.jdbc.driver.OracleDriver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"Oracle");
                        }
                        else if (prop.getProperty("driver").equalsIgnoreCase("org.postgresql.Driver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"Postgre SQL");
                        }
                        this.tfUrlBase.setText(prop.getProperty("url"));
                        this.tfUserBase.setText(prop.getProperty("login"));
                        this.tfMdpBase.setText(prop.getProperty("password"));
                    }
                    finally {
                        if (input != null) {
                            input.close();
                        }
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                }
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }
    
    @FXML
    void connexionBDD(final ActionEvent event) {
        if (this.universite != null) {
            final Alert alerte = new Alert(Alert.AlertType.INFORMATION);
            alerte.setTitle("D\u00e9j\u00e0 connect\u00e9");
            alerte.setHeaderText((String)null);
            alerte.setContentText("Vous \u00eates d\u00e9j\u00e0 connect\u00e9 \u00e0 la base de donn\u00e9es SGCE");
            alerte.showAndWait();
            return;
        }
        this.sauvegardeBaseDeDonnees(null);
        try {
            Bd baseDD;
            if (this.tfFichierConfig.getText().equals("configBDD.properties")) {
                baseDD = new Bd("./" + this.tfFichierConfig.getText());
            }
            else {
                baseDD = new Bd(this.tfFichierConfig.getText());
            }
            final Connection con = baseDD.getConnection();
            if (con != null) {
                final Alert alerte2 = new Alert(Alert.AlertType.INFORMATION);
                alerte2.setTitle("Connexion r\u00e9ussie");
                alerte2.setHeaderText((String)null);
                alerte2.setContentText("Connexion avec la base de donn\u00e9es SGCE \u00e9tablie");
                alerte2.showAndWait();
            }
            this.universite = new UniversiteSgce();
            try {
                (this.dao = new Dao(new Bd(this.tfFichierConfig.getText()))).putAllDepartements(this.universite);
                this.dao.putAllMentions(this.universite);
                this.dao.putAllFormations(this.universite);
                this.dao.putAllParcours(this.universite);
                this.dao.putAllBlocsChoix(this.universite);
                this.dao.putAllUes(this.universite);
                this.dao.putAllBlocsSousChoix(this.universite);
                this.dao.putAllEcues(this.universite);
                this.dao.putAllTypesCours(this.universite);
                this.dao.putAllHeures(this.universite);
                this.dao.putRelationMD(this.universite);
                this.dao.putRelationUeBC(this.universite);
                this.dao.putRelationEcueBSC(this.universite);
            }
            catch (Exception ex) {}
            this.initialisationComboBoxSGCEApogee();
        }
        catch (ClassNotFoundException e) {
            final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
            alerte2.setTitle("Driver manquant ou incorrect");
            alerte2.setHeaderText((String)null);
            alerte2.setContentText("Le driver de la base de donn\u00e9es n'a pas pu \u00eatre charg\u00e9");
            alerte2.showAndWait();
        }
        catch (IOException e2) {
            final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
            alerte2.setTitle("Fichier inexistant ou illisible");
            alerte2.setHeaderText((String)null);
            alerte2.setContentText("Le fichier configuration de la base de donn\u00e9es est inexistant ou n'est pas lisible");
            alerte2.showAndWait();
        }
        catch (SQLException e3) {
            final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
            alerte2.setTitle("Echec de connexion");
            alerte2.setHeaderText((String)null);
            alerte2.setContentText("La connexion \u00e0 la base de donn\u00e9es a \u00e9chou\u00e9, veuillez r\u00e9essayer");
            alerte2.showAndWait();
        }
    }
    
    @FXML
    void sauvegardeBaseDeDonnees(final ActionEvent event) {
        if (this.fichierConfig.exists()) {
            final Properties prop = new Properties();
            try {
                Throwable t = null;
                try {
                    final FileOutputStream output = new FileOutputStream(this.fichierConfig);
                    try {
                        if (((String)this.cbbTypeBase.getSelectionModel().getSelectedItem()).equalsIgnoreCase("mysql")) {
                            prop.setProperty("driver", "com.mysql.jdbc.Driver");
                        }
                        else if (((String)this.cbbTypeBase.getSelectionModel().getSelectedItem()).equalsIgnoreCase("sql server")) {
                            prop.setProperty("driver", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
                        }
                        else if (((String)this.cbbTypeBase.getSelectionModel().getSelectedItem()).equalsIgnoreCase("postgre sql")) {
                            prop.setProperty("driver", "org.postgresql.Driver");
                        }
                        else if (((String)this.cbbTypeBase.getSelectionModel().getSelectedItem()).equalsIgnoreCase("oracle")) {
                            prop.setProperty("driver", "oracle.jdbc.driver.OracleDriver");
                        }
                        prop.setProperty("url", this.tfUrlBase.getText());
                        prop.setProperty("login", this.tfUserBase.getText());
                        prop.setProperty("password", this.tfMdpBase.getText());
                        prop.store(output, null);
                    }
                    finally {
                        if (output != null) {
                            output.close();
                        }
                    }
                }
                finally {
                    if (t == null) {
                        final Throwable exception;
                        t = exception;
                    }
                    else {
                        final Throwable exception;
                        if (t != exception) {
                            t.addSuppressed(exception);
                        }
                    }
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            final Alert alerte = new Alert(Alert.AlertType.INFORMATION);
            alerte.setTitle("Sauvegarde des informations");
            alerte.setHeaderText((String)null);
            alerte.setContentText("La sauvegarde des informations de la base de donn\u00e9es SGCE\n\u00e0 \u00e9t\u00e9 effectu\u00e9e avec succ\u00e8s.");
            alerte.showAndWait();
        }
        else {
            final Alert alerte2 = new Alert(Alert.AlertType.ERROR);
            alerte2.setTitle("Sauvegarde des informations");
            alerte2.setHeaderText((String)null);
            alerte2.setContentText("La sauvegarde des informations de la base de donn\u00e9es SGCE\na \u00e9chou\u00e9, v\u00e9rifiez que le fichier configuration existe");
            alerte2.showAndWait();
        }
    }
    
    public void init(final FenetrePrincipale f) {
        this.fenetre = f;
        final File file = new File(this.getClass().getClassLoader().getResource("logo_DESCARTES_transparant.png").toString());
        final Image image = new Image(file.toURI().toString());
        this.ivLogoDescartes.setImage(image);
        this.fichierConfig = new File("./configBDD.properties");
        this.tfFichierConfig.setEditable(false);
        this.tfFichierConfig.setText("configBDD.properties");
        this.tfChoixDossierApogee.setPromptText("URL du dossier");
        this.tfChoixDossierApogee.setEditable(false);
        this.tfDossierSortieApogee.setText("Dossier V\u00e9rification Insertion Apog\u00e9e");
        this.tfDossierSortieApogee.setEditable(false);
        this.tfDossierSortieSGCE.setText("Dossier Traitement SGCE");
        this.tfDossierSortieSGCE.setEditable(false);
        this.cbbChoixFormationApogee.setDisable(true);
        this.cbbChoixFormationComparaisonApogee.setDisable(true);
        Platform.runLater(() -> this.intitialisationComboboxTypeBase());
        Platform.runLater(() -> this.initialisationFichierConfig());
        if (!new File("./" + this.tfDossierSortieSGCE.getText()).exists()) {
            new File("./" + this.tfDossierSortieSGCE.getText()).mkdir();
        }
        if (!new File("./" + this.tfDossierSortieApogee.getText()).exists()) {
            new File("./" + this.tfDossierSortieApogee.getText()).mkdir();
        }
    }
    
    private void intitialisationComboboxTypeBase() {
        this.cbbTypeBase.getItems().add((Object)"Postgre SQL");
        this.cbbTypeBase.getItems().add((Object)"MySQL");
        this.cbbTypeBase.getItems().add((Object)"SQL Server");
        this.cbbTypeBase.getItems().add((Object)"Oracle");
    }
    
    private void initialisationComboBoxSGCEApogee() {
        if (this.universite != null) {
            this.initialisationComboBoxSGCE();
            this.initialisationComboBoxApogee();
        }
        else {
            final Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setTitle("Non connect\u00e9 \u00e0 la base de donn\u00e9es");
            alerte.setHeaderText((String)null);
            alerte.setContentText("Vous n'\u00eates pas connect\u00e9 \u00e0 la base de donn\u00e9es.\nAllez dans la partie \"R\u00e9glage Base SGCE\" afin de vous connecter");
            alerte.showAndWait();
        }
    }
    
    private void initialisationComboBoxSGCE() {
        this.cbbChoixUFRSGCE.getItems().clear();
        final ArrayList<Departement> departements = this.universite.getTousLesDepartements();
        for (final Departement d : departements) {
            this.cbbChoixUFRSGCE.getItems().add((Object)d.getNom());
        }
        this.initialisationColonnes();
        this.initialisationTableau();
        this.cbbChoixUFRSGCE.setOnAction((EventHandler)new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent e) {
                ControllerFenetre.this.cbbChoixFormationSGCE.getItems().clear();
                ControllerFenetre.this.cbbChoixFormationSGCE.getSelectionModel().select(-1);
                ControllerFenetre.this.cbbChoixParcoursSGCE.getItems().clear();
                ControllerFenetre.this.cbbChoixParcoursSGCE.getSelectionModel().select(-1);
                ControllerFenetre.this.initialisationColonnes();
                ControllerFenetre.this.initialisationTableau();
                ControllerFenetre.this.cbbChoixFormationSGCE.setDisable(false);
                if (ControllerFenetre.this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1) {
                    final ArrayList<Mention> mentionsUFR = departements.get(ControllerFenetre.this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex()).getListeMention();
                    final ArrayList<sgce.elps.Formation> formations = new ArrayList<sgce.elps.Formation>();
                    for (final Mention m : mentionsUFR) {
                        final ArrayList<sgce.elps.Formation> uneFormation = m.getListeFormation();
                        formations.addAll(uneFormation);
                        for (final sgce.elps.Formation f : uneFormation) {
                            ControllerFenetre.this.cbbChoixFormationSGCE.getItems().add((Object)f.getNom());
                        }
                    }
                    ControllerFenetre.this.cbbChoixFormationSGCE.setOnAction((EventHandler)new EventHandler<ActionEvent>() {
                        public void handle(final ActionEvent e) {
                            ControllerFenetre.this.cbbChoixParcoursSGCE.getItems().clear();
                            ControllerFenetre.this.cbbChoixParcoursSGCE.getSelectionModel().select(-1);
                            ControllerFenetre.this.listeFichiers.clear();
                            ControllerFenetre.this.initialisationColonnes();
                            ControllerFenetre.this.initialisationTableau();
                            ControllerFenetre.this.cbbChoixParcoursSGCE.setDisable(false);
                            if (ControllerFenetre.this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1) {
                                final ArrayList<Parcours> parcours = formations.get(ControllerFenetre.this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex()).getListeParcours();
                                for (final Parcours p : parcours) {
                                    ControllerFenetre.this.cbbChoixParcoursSGCE.getItems().add((Object)p.getNom());
                                }
                                ControllerFenetre.this.cbbChoixParcoursSGCE.setOnAction((EventHandler)new EventHandler<ActionEvent>() {
                                    public void handle(final ActionEvent e) {
                                        ControllerFenetre.this.listeFichiers.clear();
                                        ControllerFenetre.this.initialisationColonnes();
                                        ControllerFenetre.this.initialisationTableau();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    
    private void initialisationFichierConfig() {
        final File fichierConfig = new File("./configBDD.properties");
        if (!fichierConfig.exists()) {
            try {
                final Alert alerte = new Alert(Alert.AlertType.CONFIRMATION);
                alerte.setTitle("Fichier introuvable");
                alerte.setHeaderText((String)null);
                alerte.setContentText("Impossible de trouver le fichier \"configBDD.properties\".\nSouhaitez-vous en cr\u00e9er un vierge ?");
                final ButtonType btnOui = new ButtonType("Oui");
                final ButtonType btnNon = new ButtonType("Non");
                alerte.getButtonTypes().setAll((Object[])new ButtonType[] { btnOui, btnNon });
                final Optional<ButtonType> btnAppuye = (Optional<ButtonType>)alerte.showAndWait();
                if (btnAppuye.get() == btnOui) {
                    fichierConfig.createNewFile();
                    final Properties prop = new Properties();
                    Throwable t = null;
                    try {
                        final FileOutputStream output = new FileOutputStream(fichierConfig);
                        try {
                            prop.setProperty("driver", "");
                            prop.setProperty("url", "");
                            prop.setProperty("login", "");
                            prop.setProperty("password", "");
                            prop.store(output, null);
                        }
                        finally {
                            if (output != null) {
                                output.close();
                            }
                        }
                    }
                    finally {
                        if (t == null) {
                            final Throwable exception;
                            t = exception;
                        }
                        else {
                            final Throwable exception;
                            if (t != exception) {
                                t.addSuppressed(exception);
                            }
                        }
                    }
                    this.tfFichierConfig.setText(fichierConfig.getAbsolutePath());
                    alerte.close();
                }
                else if (btnAppuye.get() == btnNon) {
                    alerte.close();
                    this.tfFichierConfig.setText("");
                    this.tfFichierConfig.setPromptText("URL du fichier");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            final Properties prop2 = new Properties();
            try {
                Throwable t2 = null;
                try {
                    final FileInputStream input = new FileInputStream("./configBDD.properties");
                    try {
                        prop2.load(input);
                        if (prop2.getProperty("driver").equalsIgnoreCase("com.mysql.jdbc.Driver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"MySQL");
                        }
                        else if (prop2.getProperty("driver").equalsIgnoreCase("com.microsoft.sqlserver.jdbc.SQLServerDriver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"SQL Server");
                        }
                        else if (prop2.getProperty("driver").equalsIgnoreCase("oracle.jdbc.driver.OracleDriver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"Oracle");
                        }
                        else if (prop2.getProperty("driver").equalsIgnoreCase("org.postgresql.Driver")) {
                            this.cbbTypeBase.getSelectionModel().select((Object)"Postgre SQL");
                        }
                        this.tfUrlBase.setText(prop2.getProperty("url"));
                        this.tfUserBase.setText(prop2.getProperty("login"));
                        this.tfMdpBase.setText(prop2.getProperty("password"));
                    }
                    finally {
                        if (input != null) {
                            input.close();
                        }
                    }
                }
                finally {
                    if (t2 == null) {
                        final Throwable exception2;
                        t2 = exception2;
                    }
                    else {
                        final Throwable exception2;
                        if (t2 != exception2) {
                            t2.addSuppressed(exception2);
                        }
                    }
                }
            }
            catch (FileNotFoundException e2) {
                e2.printStackTrace();
            }
            catch (IOException e3) {
                e3.printStackTrace();
            }
        }
    }
    
    private void initialisationTableau() {
        this.listeFichiers = (ObservableList<InformationsFichier>)FXCollections.observableArrayList();
        this.initialisationColonnes();
        this.recuperationInformationsFichiers();
    }
    
    private void initialisationComboBoxApogee() {
        this.cbbChoixUFRApogee.getItems().clear();
        final ArrayList<Departement> departements = this.universite.getTousLesDepartements();
        for (final Departement d : departements) {
            this.cbbChoixUFRApogee.getItems().add((Object)d.getNom());
        }
        this.cbbChoixFormationApogee.getSelectionModel().select(-1);
        this.cbbChoixUFRApogee.setOnAction((EventHandler)new EventHandler<ActionEvent>() {
            public void handle(final ActionEvent e) {
                ControllerFenetre.this.cbbChoixFormationApogee.getSelectionModel().select(-1);
                ControllerFenetre.this.cbbChoixFormationApogee.getItems().clear();
                if (ControllerFenetre.this.cbbChoixUFRApogee.getSelectionModel().getSelectedIndex() != -1) {
                    final ArrayList<Mention> mentions = departements.get(ControllerFenetre.this.cbbChoixUFRApogee.getSelectionModel().getSelectedIndex()).getListeMention();
                    ControllerFenetre.this.cbbChoixFormationApogee.setDisable(false);
                    for (final Mention m : mentions) {
                        for (final sgce.elps.Formation f : m.getListeFormation()) {
                            ControllerFenetre.this.cbbChoixFormationApogee.getItems().add((Object)f.getNom());
                        }
                    }
                    ControllerFenetre.this.cbbChoixFormationApogee.setOnAction((EventHandler)new EventHandler<ActionEvent>() {
                        public void handle(final ActionEvent e) {
                            if (ControllerFenetre.this.cbbChoixFormationApogee.getSelectionModel().getSelectedIndex() != -1) {
                                ControllerFenetre.this.tfNomFichierSortieApogee.setText((String)ControllerFenetre.this.cbbChoixFormationApogee.getSelectionModel().getSelectedItem());
                            }
                        }
                    });
                }
                else {
                    ControllerFenetre.this.cbbChoixFormationApogee.setDisable(true);
                    ControllerFenetre.this.cbbChoixFormationApogee.getSelectionModel().select(-1);
                    ControllerFenetre.this.cbbChoixFormationApogee.getItems().clear();
                }
            }
        });
    }
    
    private void initialisationColonnes() {
        if (this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() != -1) {
            this.colonne1.setText("Parcours");
            this.colonne1.setCellValueFactory((Callback)new PropertyValueFactory("colonne1"));
            this.colonne2.setText("Code");
            this.colonne2.setCellValueFactory((Callback)new PropertyValueFactory("colonne2"));
            this.colonne3.setText("Nom du fichier");
            this.colonne3.setOnEditCommit(t -> ((InformationsFichier)t.getTableView().getItems().get(t.getTablePosition().getRow())).setColonne3((String)t.getNewValue()));
            this.colonne3.setCellFactory(TextFieldTableCell.forTableColumn());
            this.colonne3.setCellValueFactory((Callback)new PropertyValueFactory("colonne3"));
            this.colonne3.setEditable(true);
        }
        else if (this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() == -1 && this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1) {
            this.colonne1.setText("Formation");
            this.colonne1.setCellValueFactory((Callback)new PropertyValueFactory("colonne1"));
            this.colonne2.setText("Acronyme");
            this.colonne2.setCellValueFactory((Callback)new PropertyValueFactory("colonne2"));
            this.colonne3.setText("Nom du fichier");
            this.colonne3.setOnEditCommit(t -> ((InformationsFichier)t.getTableView().getItems().get(t.getTablePosition().getRow())).setColonne3((String)t.getNewValue()));
            this.colonne3.setCellFactory(TextFieldTableCell.forTableColumn());
            this.colonne3.setCellValueFactory((Callback)new PropertyValueFactory("colonne3"));
            this.colonne3.setEditable(true);
        }
        else if (this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() == -1 && this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1) {
            this.colonne1.setText("UFR");
            this.colonne1.setCellValueFactory((Callback)new PropertyValueFactory("colonne1"));
            this.colonne2.setText("Acronyme");
            this.colonne2.setCellValueFactory((Callback)new PropertyValueFactory("colonne2"));
            this.colonne3.setText("Nom du fichier");
            this.colonne3.setOnEditCommit(t -> ((InformationsFichier)t.getTableView().getItems().get(t.getTablePosition().getRow())).setColonne3((String)t.getNewValue()));
            this.colonne3.setCellFactory(TextFieldTableCell.forTableColumn());
            this.colonne3.setCellValueFactory((Callback)new PropertyValueFactory("colonne3"));
            this.colonne3.setEditable(true);
        }
        else if (this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() == -1) {
            this.colonne1.setText("UFR");
            this.colonne1.setCellValueFactory((Callback)new PropertyValueFactory("colonne1"));
            this.colonne2.setText("Acronyme");
            this.colonne2.setCellValueFactory((Callback)new PropertyValueFactory("colonne2"));
            this.colonne3.setText("Nom du fichier");
            this.colonne3.setOnEditCommit(t -> ((InformationsFichier)t.getTableView().getItems().get(t.getTablePosition().getRow())).setColonne3((String)t.getNewValue()));
            this.colonne3.setCellFactory(TextFieldTableCell.forTableColumn());
            this.colonne3.setCellValueFactory((Callback)new PropertyValueFactory("colonne3"));
            this.colonne3.setEditable(true);
        }
        this.tableauFichiers.setEditable(true);
        this.tableauFichiers.getColumns().clear();
        this.tableauFichiers.getColumns().add((Object)this.colonne1);
        this.tableauFichiers.getColumns().add((Object)this.colonne2);
        this.tableauFichiers.getColumns().add((Object)this.colonne3);
    }
    
    private void recuperationInformationsFichiers() {
        this.listeFichiers.clear();
        final ArrayList<Departement> departements = this.universite.getTousLesDepartements();
        final ArrayList<sgce.elps.Formation> formations = new ArrayList<sgce.elps.Formation>();
        if (this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() != -1) {
            final ArrayList<Mention> mentions = departements.get(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex()).getListeMention();
            for (final Mention m : mentions) {
                formations.addAll(m.getListeFormation());
            }
            final ArrayList<Parcours> parcours = formations.get(this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex()).getListeParcours();
            if (parcours.isEmpty()) {
                final sgce.elps.Formation f = formations.get(this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex());
                this.listeFichiers.add((Object)new InformationsFichier(f.getNom(), f.getAcronyme(), "Fichier " + f.getNom() + ".pdf"));
                final Alert alerte = new Alert(Alert.AlertType.WARNING);
                alerte.setTitle("Pas de parcours");
                alerte.setHeaderText((String)null);
                alerte.setContentText("Il n'y a pas de Parcours sp\u00e9cifi\u00e9s dans cette Formation");
                alerte.showAndWait();
            }
            else if (this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() != -1) {
                final Parcours p = parcours.get(this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex());
                this.listeFichiers.add((Object)new InformationsFichier(p.getNom(), p.getCode(), "Fichier " + p.getNom() + ".pdf"));
            }
            else {
                for (final Parcours p : parcours) {
                    this.listeFichiers.add((Object)new InformationsFichier(p.getNom(), p.getCode(), "Fichier " + p.getNom()));
                }
            }
        }
        else if (this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixParcoursSGCE.getSelectionModel().getSelectedIndex() == -1) {
            final ArrayList<Mention> mentions = departements.get(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex()).getListeMention();
            for (final Mention m : mentions) {
                formations.addAll(m.getListeFormation());
            }
            sgce.elps.Formation formation = null;
            for (final sgce.elps.Formation f2 : formations) {
                if (f2.getNom().equals(this.cbbChoixFormationSGCE.getSelectionModel().getSelectedItem())) {
                    formation = f2;
                }
            }
            this.listeFichiers.add((Object)new InformationsFichier(formation.getNom(), formation.getAcronyme(), "Fichier " + formation.getNom() + ".pdf"));
        }
        else if (this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() != -1 && this.cbbChoixFormationSGCE.getSelectionModel().getSelectedIndex() == -1) {
            Departement departement = null;
            for (final Departement d : this.universite.getTousLesDepartements()) {
                if (d.getNom().equals(this.cbbChoixUFRSGCE.getSelectionModel().getSelectedItem())) {
                    departement = d;
                }
            }
            this.listeFichiers.add((Object)new InformationsFichier(departement.getNom(), departement.getAcronyme(), "Fichier " + departement.getNom() + ".pdf"));
        }
        else if (this.cbbChoixUFRSGCE.getSelectionModel().getSelectedIndex() == -1) {
            for (final Departement d2 : departements) {
                this.listeFichiers.add((Object)new InformationsFichier(d2.getNom(), d2.getAcronyme(), "Fichier " + d2.getNom() + ".pdf"));
            }
        }
        this.tableauFichiers.setItems((ObservableList)this.listeFichiers);
    }
}
