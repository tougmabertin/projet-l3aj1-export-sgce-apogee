// 
// Decompiled by Procyon v0.5.36
// 

package ui;

import javafx.beans.property.SimpleStringProperty;

public class InformationsFichier
{
    private SimpleStringProperty colonne1;
    private SimpleStringProperty colonne2;
    private SimpleStringProperty colonne3;
    
    public InformationsFichier(final String c1, final String c2, final String c3) {
        this.colonne1 = new SimpleStringProperty(c1);
        this.colonne2 = new SimpleStringProperty(c2);
        this.colonne3 = new SimpleStringProperty(c3);
    }
    
    public String getColonne1() {
        return this.colonne1.get();
    }
    
    public void setColonne1(final String colonne1) {
        this.colonne1.set(colonne1);
    }
    
    public String getColonne2() {
        return this.colonne2.get();
    }
    
    public void setColonne2(final String colonne2) {
        this.colonne2.set(colonne2);
    }
    
    public String getColonne3() {
        return this.colonne3.get();
    }
    
    public void setColonne3(final String colonne3) {
        this.colonne3.set(colonne3);
    }
}
