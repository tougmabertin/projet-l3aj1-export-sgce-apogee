// 
// Decompiled by Procyon v0.5.36
// 

package ui;

import javafx.stage.WindowEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.application.Application;

public class FenetrePrincipale extends Application
{
    private BorderPane panel;
    
    public void start(final Stage stage) throws Exception {
        final FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getClassLoader().getResource("InterfaceGraphique.fxml"));
        final Node node = (Node)fxmlLoader.load();
        (this.panel = new BorderPane(node)).setPrefSize(500.0, 475.0);
        final ControllerFenetre controller = (ControllerFenetre)fxmlLoader.getController();
        controller.init(this);
        final Scene scene = new Scene((Parent)this.panel);
        stage.centerOnScreen();
        stage.setResizable(false);
        stage.setTitle("Application G\u00e9n\u00e9rale");
        stage.setScene(scene);
        stage.sizeToScene();
        stage.setOnCloseRequest(event -> stage.close());
        stage.show();
    }
    
    public static void main(final String[] args) {
        launch(args);
    }
    
    public void setDisable(final boolean b) {
        this.panel.setDisable(b);
    }
}
