// 
// Decompiled by Procyon v0.5.36
// 

package ui;

import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.control.TableCell;

public class ModificationCellule extends TableCell<InformationsFichier, String>
{
    private TextField textField;
    
    public void startEdit() {
        if (!this.isEmpty()) {
            super.startEdit();
            this.creationTextfield();
            this.setText((String)null);
            this.setGraphic((Node)this.textField);
            this.textField.selectAll();
        }
    }
    
    public void cancelEdit() {
        super.cancelEdit();
        this.setText((String)this.getItem());
        this.setGraphic((Node)null);
    }
    
    public void updateItem(final String item, final boolean textFieldRemplie) {
        super.updateItem((Object)item, textFieldRemplie);
        if (textFieldRemplie) {
            this.setText((String)null);
            this.setGraphic((Node)null);
        }
        else if (this.isEditing()) {
            if (this.textField != null) {
                this.textField.setText(this.getTexte());
            }
            this.setText((String)null);
            this.setGraphic((Node)this.textField);
        }
        else {
            this.setText(this.getTexte());
            this.setGraphic((Node)null);
        }
    }
    
    private void creationTextfield() {
        (this.textField = new TextField(this.getTexte())).setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2.0);
        this.textField.focusedProperty().addListener((arg0, arg1, arg2) -> {
            if (!arg2) {
                this.commitEdit((Object)this.textField.getText());
            }
        });
    }
    
    private String getTexte() {
        if (this.getItem() == null) {
            return "";
        }
        if (((String)this.getItem()).endsWith(".pdf")) {
            return (String)this.getItem();
        }
        return String.valueOf(this.getItem()) + ".pdf";
    }
}
