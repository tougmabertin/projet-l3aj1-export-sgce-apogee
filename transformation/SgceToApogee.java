// 
// Decompiled by Procyon v0.5.36
// 

package transformation;

import sgce.elps.Obligatoire;
import sgce.elps.Ecue;
import java.util.LinkedHashMap;
import sgce.elps.BlocSousChoix;
import apogee.Heure.Heure;
import sgce.elps.Ue;
import sgce.elps.BlocChoix;
import sgce.liste.Semestre;
import apogee.elp.TypeElp;
import apogee.elp.ElpApogee;
import sgce.elps.Parcours;
import apogee.liste.ListeApogee;
import apogee.liste.TypeListe;
import java.util.Iterator;
import sgce.elps.Formation;
import sgce.elps.Mention;
import sgce.elps.Departement;
import sgce.universite.UniversiteSgce;
import java.util.Random;
import apogee.universite.UniversiteApogee;
import java.util.ArrayList;

public class SgceToApogee
{
    private static ArrayList<String> codeElpExistants;
    private UniversiteApogee univA;
    
    static {
        SgceToApogee.codeElpExistants = new ArrayList<String>();
    }
    
    public SgceToApogee(final UniversiteApogee univA) {
        this.univA = univA;
    }
    
    private static boolean estUnChiffre(final char c) {
        return Character.isDigit(c);
    }
    
    private static String genererCodeElp(final String codePere) {
        String codeElp = "";
        final Random rand = new Random();
        if (codePere != null && codePere.length() >= 1) {
            if (codePere.length() < 5) {
                codeElp = codePere;
                while (codeElp.length() < 5) {
                    if (estUnChiffre(codeElp.charAt(codeElp.length() - 1))) {
                        final int lettre = rand.nextInt(26);
                        codeElp = String.valueOf(codeElp) + String.valueOf((char)(lettre + 65));
                    }
                    else {
                        codeElp = String.valueOf(codeElp) + rand.nextInt(10);
                    }
                }
            }
            else if (codePere.length() < 8) {
                if (estUnChiffre(codePere.charAt(codePere.length() - 1))) {
                    final int lettre = rand.nextInt(26);
                    codeElp = String.valueOf(codeElp) + codePere + String.valueOf((char)(lettre + 65));
                }
                else {
                    codeElp = String.valueOf(codeElp) + codePere + rand.nextInt(10);
                }
            }
            else if (codePere.length() >= 8) {
                if (estUnChiffre(codePere.charAt(codePere.length() - 2))) {
                    final int lettre = rand.nextInt(26);
                    codeElp = codePere.substring(0, codePere.length() - 1);
                    codeElp = String.valueOf(codeElp) + String.valueOf((char)(lettre + 65));
                }
                else {
                    codeElp = codePere.substring(0, codePere.length() - 1);
                    codeElp = String.valueOf(codeElp) + rand.nextInt(10);
                }
            }
        }
        else {
            final int lettre = rand.nextInt(26);
            codeElp = String.valueOf(codeElp) + String.valueOf((char)lettre + 'A');
            while (codeElp.length() < 5) {
                if (estUnChiffre(codeElp.charAt(codeElp.length() - 1))) {
                    final int lettre2 = rand.nextInt(26);
                    codeElp = String.valueOf(codeElp) + String.valueOf((char)(lettre2 + 65));
                }
                else {
                    codeElp = String.valueOf(codeElp) + rand.nextInt(10);
                }
            }
        }
        return codeElp;
    }
    
    private static String genererCodeListe(final String codePere) {
        String codeListe = "";
        final Random rand = new Random();
        if (codePere != null && codePere.length() >= 1) {
            if (codePere.length() < 5) {
                codeListe = codePere;
                while (codeListe.length() < 5) {
                    if (estUnChiffre(codeListe.charAt(codeListe.length() - 1))) {
                        final int lettre = rand.nextInt(26);
                        codeListe = String.valueOf(codeListe) + String.valueOf((char)(lettre + 65));
                    }
                    else {
                        codeListe = String.valueOf(codeListe) + rand.nextInt(10);
                    }
                }
            }
            else if (codePere.length() < 8) {
                if (estUnChiffre(codePere.charAt(codePere.length() - 1))) {
                    final int lettre = rand.nextInt(26);
                    codeListe = String.valueOf(codeListe) + codePere + String.valueOf((char)(lettre + 65));
                }
                else {
                    codeListe = String.valueOf(codeListe) + codePere + rand.nextInt(10);
                }
            }
            else if (estUnChiffre(codePere.charAt(codePere.length() - 1))) {
                final int lettre = rand.nextInt(26);
                codeListe = codePere.substring(0, codePere.length() - 1);
                codeListe = String.valueOf(codeListe) + String.valueOf((char)(lettre + 65));
            }
            else {
                codeListe = codePere.substring(0, codePere.length() - 1);
                codeListe = String.valueOf(codeListe) + rand.nextInt(10);
            }
        }
        else {
            final int lettre = rand.nextInt(26);
            codeListe = String.valueOf(codeListe) + String.valueOf((char)(lettre + 65));
            while (codeListe.length() < 5) {
                if (estUnChiffre(codeListe.charAt(codeListe.length() - 1))) {
                    final int lettre2 = rand.nextInt(26);
                    codeListe = String.valueOf(codeListe) + String.valueOf((char)(lettre2 + 65));
                }
                else {
                    codeListe = String.valueOf(codeListe) + rand.nextInt(10);
                }
            }
        }
        return codeListe;
    }
    
    public UniversiteApogee transformerArborescence(final UniversiteSgce univS) {
        for (final Departement dep : univS.getTousLesDepartements()) {
            for (final Mention m : dep.getListeMention()) {
                for (final Formation f : m.getListeFormation()) {
                    this.transformationFormation(f);
                }
            }
        }
        return this.univA;
    }
    
    public apogee.formation.Formation transformationFormation(final Formation formation) {
        SgceToApogee.codeElpExistants.add(formation.getCode());
        final apogee.formation.Formation f = new apogee.formation.Formation(String.valueOf(formation.getVersion()), formation.getCode(), formation.getAcronyme(), formation.getNom());
        final ListeApogee listeForm = new ListeApogee(genererCodeListe(formation.getCode()), TypeListe.O, "Liste " + formation.getNom());
        for (final Parcours p : formation.getListeParcours()) {
            final ElpApogee parcours = this.transformationParcours(p);
            parcours.setCodePere(listeForm.getCodeLse());
            if (parcours != null) {
                listeForm.addListeElp(parcours);
            }
        }
        f.addListeApogee(listeForm);
        this.univA.addFormation(f);
        this.univA.addListeApogee(listeForm);
        return f;
    }
    
    public ElpApogee transformationParcours(final Parcours parcours) {
        SgceToApogee.codeElpExistants.add(parcours.getCode());
        final ElpApogee p = new ElpApogee(parcours.getCode(), parcours.getNom(), TypeElp.PARCOURS, String.valueOf(parcours.getEcts()));
        ListeApogee listeP = null;
        if (parcours.getListeSemestre().getTypeListe() == null) {
            listeP = new ListeApogee(genererCodeListe(parcours.getCode()), TypeListe.O, "Liste " + parcours.getNom());
        }
        else {
            switch (parcours.getListeSemestre().getTypeListe()) {
                case O: {
                    listeP = new ListeApogee(genererCodeListe(parcours.getCode()), TypeListe.O, "Liste " + parcours.getNom());
                }
                case OC: {
                    listeP = new ListeApogee(genererCodeListe(parcours.getCode()), TypeListe.OC, "Liste " + parcours.getNom());
                }
                case F: {
                    listeP = new ListeApogee(genererCodeListe(parcours.getCode()), TypeListe.F, "Liste " + parcours.getNom());
                    break;
                }
            }
            listeP = new ListeApogee(genererCodeListe(parcours.getCode()), TypeListe.O, "Liste " + parcours.getNom());
        }
        for (final Semestre s : parcours.getListeSemestre().getListe()) {
            final ElpApogee semestre = this.transformationSemestre(s);
            semestre.setCodePere(listeP.getCodeLse());
            if (semestre != null) {
                listeP.addListeElp(semestre);
            }
        }
        if (!listeP.getListe().isEmpty()) {
            p.addListeApogee(listeP);
        }
        this.univA.addElpApogee(p);
        this.univA.addListeApogee(listeP);
        return p;
    }
    
    private ElpApogee transformationSemestre(final Semestre semestre) {
        final ElpApogee s = new ElpApogee(genererCodeElp(semestre.getCodePere()), semestre.getNom(), TypeElp.SEMESTRE, String.valueOf(semestre.getEcts()));
        ListeApogee listeS = null;
        if (semestre.getListeBc().getTypeListe() == null) {
            listeS = new ListeApogee(genererCodeListe(s.getCodeElp()), TypeListe.O, "Liste " + semestre.getNom());
        }
        else {
            switch (semestre.getListeBc().getTypeListe()) {
                case O: {
                    listeS = new ListeApogee(genererCodeListe(s.getCodeElp()), TypeListe.O, "Liste " + semestre.getNom());
                }
                case OC: {
                    listeS = new ListeApogee(genererCodeListe(s.getCodeElp()), TypeListe.OC, "Liste " + semestre.getNom());
                }
                case F: {
                    listeS = new ListeApogee(genererCodeListe(s.getCodeElp()), TypeListe.F, "Liste " + semestre.getNom());
                    break;
                }
            }
        }
        for (final BlocChoix bc : semestre.getListeBc().getListe()) {
            final ElpApogee blocChoix = this.transformationBlocChoix(bc);
            blocChoix.setCodePere(listeS.getCodeLse());
            if (blocChoix != null) {
                listeS.addListeElp(blocChoix);
            }
        }
        if (!listeS.getListe().isEmpty()) {
            s.addListeApogee(listeS);
        }
        this.univA.addElpApogee(s);
        this.univA.addListeApogee(listeS);
        return s;
    }
    
    private ElpApogee transformationBlocChoix(final BlocChoix bc) {
        SgceToApogee.codeElpExistants.add(bc.getCode());
        ElpApogee bloc = null;
        String code;
        if (bc.getCode() != null) {
            code = bc.getCode();
        }
        else {
            code = genererCodeElp(bc.getCode());
        }
        if (bc.getNom().trim().length() > 1) {
            bloc = new ElpApogee(code, String.valueOf(bc.getNom()) + " (Choix " + bc.getNbUes() + " UE(s) parmis " + bc.getTotalUes() + ")", TypeElp.BLOC, String.valueOf(bc.getEcts()));
        }
        else {
            bloc = new ElpApogee(code, "Choix " + bc.getNbUes() + " UE(s) parmis " + bc.getTotalUes(), TypeElp.BLOC, String.valueOf(bc.getEcts()));
        }
        ListeApogee listeBloc = null;
        if (bc.getListeUe().getTypeListe() == null) {
            listeBloc = new ListeApogee(genererCodeListe(bc.getCode()), TypeListe.O, "Liste " + bc.getNom());
        }
        else {
            switch (bc.getListeUe().getTypeListe()) {
                case O: {
                    listeBloc = new ListeApogee(genererCodeListe(bc.getCode()), TypeListe.O, "Liste " + bc.getNom());
                }
                case OC: {
                    listeBloc = new ListeApogee(genererCodeListe(bc.getCode()), TypeListe.OC, "Liste " + bc.getNom());
                }
                case F: {
                    listeBloc = new ListeApogee(genererCodeListe(bc.getCode()), TypeListe.F, "Liste " + bc.getNom());
                    break;
                }
            }
            listeBloc = new ListeApogee(genererCodeListe(bc.getCode()), TypeListe.O, "Liste " + bc.getNom());
        }
        if (bc.getListeUe().getListe().size() > 1) {
            for (final Ue ue : bc.getListeUe().getListe()) {
                final ElpApogee u = this.transformationUe(ue);
                u.setCodePere(listeBloc.getCodeLse());
                if (u != null) {
                    listeBloc.addListeElp(u);
                }
            }
            if (!listeBloc.getListe().isEmpty()) {
                bloc.addListeApogee(listeBloc);
            }
            this.univA.addElpApogee(bloc);
            this.univA.addListeApogee(listeBloc);
            return bloc;
        }
        Ue ue = bc.getListeUe().getListe().get(0);
        final ElpApogee u2 = this.transformationUe(ue);
        u2.setCodePere(listeBloc.getCodeLse());
        if (u2.getNom().equalsIgnoreCase(ue.getBlocChoix().getNom())) {
            return null;
        }
        return u2;
    }
    
    private ElpApogee transformationUe(final Ue ue) {
        SgceToApogee.codeElpExistants.add(ue.getCode());
        final ElpApogee u = new ElpApogee(ue.getCode(), ue.getNom(), TypeElp.UE, String.valueOf(ue.getEcts()));
        ListeApogee listeUe = null;
        if (ue.getListeBsc().getTypeListe() == null) {
            listeUe = new ListeApogee(genererCodeListe(ue.getCode()), TypeListe.O, "Liste " + ue.getNom());
        }
        else {
            switch (ue.getListeBsc().getTypeListe()) {
                case O: {
                    listeUe = new ListeApogee(genererCodeListe(ue.getCode()), TypeListe.O, "Liste " + ue.getNom());
                }
                case OC: {
                    listeUe = new ListeApogee(genererCodeListe(ue.getCode()), TypeListe.OC, "Liste " + ue.getNom());
                }
                case F: {
                    listeUe = new ListeApogee(genererCodeListe(ue.getCode()), TypeListe.F, "Liste " + ue.getNom());
                    break;
                }
            }
            listeUe = new ListeApogee(genererCodeListe(ue.getCode()), TypeListe.O, "Liste " + ue.getNom());
        }
        final LinkedHashMap<String, String> heures = ue.getHeuresCours();
        final Heure heure = new Heure(ue.getCode(), heures.get("CM"), heures.get("TD"), heures.get("TP"), heures.get("Projet"), heures.get("Stage"), heures.get("TDM"));
        u.setHeures(heure);
        for (final BlocSousChoix bsc : ue.getListeBsc().getListe()) {
            final ElpApogee bloc = this.transformationBlocSousChoix(bsc);
            if (bloc != null) {
                listeUe.addListeElp(bloc);
            }
        }
        if (!listeUe.getListe().isEmpty()) {
            u.addListeApogee(listeUe);
        }
        this.univA.addElpApogee(u);
        this.univA.addListeApogee(listeUe);
        this.univA.addHeure(heure);
        return u;
    }
    
    private ElpApogee transformationBlocSousChoix(final BlocSousChoix bsc) {
        ElpApogee bloc = null;
        if (bsc.getNom().trim().length() > 1) {
            bloc = new ElpApogee(genererCodeElp(new StringBuilder().append(bsc.getId()).toString()), String.valueOf(bsc.getNom()) + " (Choix " + bsc.getNbEcues() + " ECUE(s) parmis " + bsc.getTotalEcues() + ")", TypeElp.BLOC);
        }
        else {
            bloc = new ElpApogee(genererCodeElp(new StringBuilder().append(bsc.getId()).toString()), "Choix " + bsc.getNbEcues() + " ECUE(s) parmis " + bsc.getTotalEcues(), TypeElp.BLOC);
        }
        ListeApogee listeBloc = null;
        if (bsc.getListeEcue().getTypeListe() == null) {
            listeBloc = new ListeApogee(genererCodeListe(bloc.getCodeElp()), TypeListe.O, "Liste " + bsc.getNom());
        }
        else {
            switch (bsc.getListeEcue().getTypeListe()) {
                case O: {
                    listeBloc = new ListeApogee(genererCodeListe(bloc.getCodeElp()), TypeListe.O, "Liste " + bsc.getNom());
                }
                case OC: {
                    listeBloc = new ListeApogee(genererCodeListe(bloc.getCodeElp()), TypeListe.OC, "Liste " + bsc.getNom());
                }
                case F: {
                    listeBloc = new ListeApogee(genererCodeListe(bloc.getCodeElp()), TypeListe.F, "Liste " + bsc.getNom());
                    break;
                }
            }
            listeBloc = new ListeApogee(genererCodeListe(bloc.getCodeElp()), TypeListe.O, "Liste " + bsc.getNom());
        }
        if (bsc.getListeEcue().getListe().size() > 1) {
            for (final Ecue ecue : bsc.getListeEcue().getListe()) {
                final ElpApogee ecu = this.transformationEcue(ecue);
                ecu.setCodePere(listeBloc.getCodeLse());
                if (ecu != null) {
                    listeBloc.addListeElp(ecu);
                }
            }
            if (!listeBloc.getListe().isEmpty()) {
                bloc.addListeApogee(listeBloc);
            }
            this.univA.addElpApogee(bloc);
            this.univA.addListeApogee(listeBloc);
            return bloc;
        }
        Ecue ecue = bsc.getListeEcue().getListe().get(0);
        final ElpApogee ecu2 = this.transformationEcue(ecue);
        ecu2.setCodePere(listeBloc.getCodeLse());
        if (ecu2.getNom().equalsIgnoreCase(bsc.getUe().getNom())) {
            return null;
        }
        return ecu2;
    }
    
    private ElpApogee transformationEcue(final Ecue ecue) {
        SgceToApogee.codeElpExistants.add(ecue.getCode());
        final LinkedHashMap<String, String> heures = ecue.getHeuresCours();
        final Heure heure = new Heure(ecue.getCode(), heures.get("CM"), heures.get("TD"), heures.get("TP"), heures.get("Projet"), heures.get("Stage"), heures.get("TDM"));
        final ElpApogee ecueA = new ElpApogee(ecue.getCode(), ecue.getAcronyme(), ecue.getNom(), TypeElp.ECUE, String.valueOf(ecue.getEcts()), heure);
        this.univA.addElpApogee(ecueA);
        this.univA.addHeure(heure);
        return ecueA;
    }
}
