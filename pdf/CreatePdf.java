// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import apogee.elp.ElpApogee;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfOutline;
import sgce.elps.Parcours;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
import transformation.SgceToApogee;
import apogee.universite.UniversiteApogee;
import sgce.elps.Formation;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.layout.Document;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import sgce.universite.UniversiteSgce;
import sgce.elps.Departement;

public class CreatePdf
{
    private String repertoire;
    
    public CreatePdf(final String repertoire) {
        this.repertoire = repertoire;
    }
    
    public void createDeptPdf(final Departement dept, final UniversiteSgce universite) throws IOException {
        final PdfWriter writer = new PdfWriter(this.repertoire);
        final PdfDocument pdfInitial = new PdfDocument(writer);
        pdfInitial.getCatalog().setPageMode(PdfName.UseOutlines);
        final Document docInitial = new Document(pdfInitial, PageSize.A3);
        docInitial.setMargins(20.0f, 50.0f, 20.0f, 50.0f);
        final PageXofY event1 = new PageXofY(pdfInitial);
        pdfInitial.addEventHandler("EndPdfPage", (IEventHandler)event1);
        final DocumentForm dForm = new DocumentForm(docInitial, dept, universite);
        final ArrayList<Integer> numPagesListe = new ArrayList<Integer>();
        dForm.setSummary(numPagesListe);
        dForm.fillDocument(numPagesListe);
        final PdfDocument pdfFinal = new PdfDocument(new PdfWriter(String.valueOf(this.repertoire) + "_Final.pdf"));
        pdfFinal.getCatalog().setPageMode(PdfName.UseOutlines);
        final Document docFinal = new Document(pdfFinal, PageSize.A3);
        docFinal.setMargins(20.0f, 50.0f, 20.0f, 50.0f);
        final PageXofY event2 = new PageXofY(pdfFinal);
        pdfFinal.addEventHandler("EndPdfPage", (IEventHandler)event2);
        final DocumentForm docForm = new DocumentForm(docFinal, dept, universite);
        docForm.setSummary(numPagesListe);
        docForm.fillDocument(numPagesListe);
        event1.writeTotal(pdfInitial);
        docInitial.close();
        new File(this.repertoire).delete();
        event2.writeTotal(pdfFinal);
        docFinal.close();
    }
    
    public void createFormationPdf(final Departement departement, final Formation formation, final UniversiteSgce universite) throws IOException {
        UniversiteApogee univApogee = new UniversiteApogee();
        final SgceToApogee transform = new SgceToApogee(univApogee);
        univApogee = transform.transformerArborescence(universite);
        final FillWithApogee fillApogee = new FillWithApogee(univApogee);
        final apogee.formation.Formation form = transform.transformationFormation(formation);
        final PdfWriter writer = new PdfWriter(this.repertoire);
        final PdfDocument pdf = new PdfDocument(writer);
        final PageXofY event = new PageXofY(pdf);
        pdf.addEventHandler("EndPdfPage", (IEventHandler)event);
        final Document doc = new Document(pdf, PageSize.A3);
        doc.setMargins(20.0f, 50.0f, 20.0f, 50.0f);
        doc.add(new Image(ImageDataFactory.create(this.getClass().getClassLoader().getResource("logo_DESCARTES.jpg"))).setWidth(300.0f).setHeight(80.0f).setMarginBottom(40.0f));
        final Paragraph text = new Paragraph(String.valueOf(departement.getNom()) + " - " + form.getNom());
        ((Paragraph)((Paragraph)((Paragraph)text.setMarginBottom(30.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setFontSize(15.0f)).setBold();
        doc.add((IBlockElement)text);
        final Table tabApogee = fillApogee.getTable();
        fillApogee.fillWithVet(tabApogee, form, pdf);
        doc.add((IBlockElement)tabApogee);
        event.writeTotal(pdf);
        doc.close();
    }
    
    public void createParcoursPdf(final Departement departement, final Formation formation, final Parcours parcours, final UniversiteSgce universite) throws IOException {
        UniversiteApogee univApogee = new UniversiteApogee();
        final SgceToApogee transform = new SgceToApogee(univApogee);
        univApogee = transform.transformerArborescence(universite);
        final FillWithApogee fillApogee = new FillWithApogee(univApogee);
        final apogee.formation.Formation form = transform.transformationFormation(formation);
        final ElpApogee elpParcours = transform.transformationParcours(parcours);
        final PdfWriter writer = new PdfWriter(this.repertoire);
        final PdfDocument pdf = new PdfDocument(writer);
        final PageXofY event = new PageXofY(pdf);
        pdf.addEventHandler("EndPdfPage", (IEventHandler)event);
        final Document doc = new Document(pdf, PageSize.A3);
        doc.setMargins(20.0f, 50.0f, 20.0f, 50.0f);
        doc.add(new Image(ImageDataFactory.create(this.getClass().getClassLoader().getResource("logo_DESCARTES.jpg"))).setWidth(300.0f).setHeight(80.0f).setMarginBottom(40.0f));
        final Paragraph text = new Paragraph(String.valueOf(departement.getNom()) + " - " + form.getNom() + " - " + elpParcours.getNom());
        ((Paragraph)((Paragraph)((Paragraph)text.setMarginBottom(30.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setFontSize(15.0f)).setBold();
        doc.add((IBlockElement)text);
        final PdfOutline root = pdf.getOutlines(false);
        final PdfOutline outlineElp = root.addOutline("Elp - " + elpParcours.getCodeElp());
        outlineElp.setStyle(PdfOutline.FLAG_BOLD);
        outlineElp.setColor(ColorConstants.BLACK);
        final Table tabApogee = fillApogee.getTable();
        fillApogee.fillWithElp(tabApogee, elpParcours, 10.0f, 15.0f, outlineElp);
        doc.add((IBlockElement)tabApogee);
        event.writeTotal(pdf);
        doc.close();
    }
}
