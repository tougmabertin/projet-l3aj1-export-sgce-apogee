// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.xobject.PdfXObject;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.Canvas;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.kernel.events.IEventHandler;

public class PageXofY implements IEventHandler
{
    protected PdfFormXObject placeholder;
    protected float side;
    protected float x;
    protected float y;
    protected float space;
    protected float descent;
    
    public PageXofY(final PdfDocument pdf) {
        this.side = 20.0f;
        this.x = 420.0f;
        this.y = 30.0f;
        this.space = 4.5f;
        this.descent = 3.0f;
        this.placeholder = new PdfFormXObject(new Rectangle(0.0f, 0.0f, this.side, this.side));
    }
    
    public void handleEvent(final Event event) {
        final PdfDocumentEvent docEvent = (PdfDocumentEvent)event;
        final PdfDocument pdf = docEvent.getDocument();
        final PdfPage page = docEvent.getPage();
        final int pageNumber = pdf.getPageNumber(page);
        final Rectangle pageSize = page.getPageSize();
        final PdfCanvas pdfCanvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdf);
        final Canvas canvas = new Canvas(pdfCanvas, pdf, pageSize);
        final Paragraph p = new Paragraph().add("Page ").add(String.valueOf(pageNumber)).add(" /");
        canvas.showTextAligned(p, this.x, this.y, TextAlignment.RIGHT);
        pdfCanvas.addXObject((PdfXObject)this.placeholder, this.x + this.space, this.y - this.descent);
        canvas.close();
        pdfCanvas.release();
    }
    
    public void writeTotal(final PdfDocument pdf) {
        final Canvas canvas = new Canvas(this.placeholder, pdf);
        canvas.showTextAligned(String.valueOf(pdf.getNumberOfPages()), 0.0f, this.descent, TextAlignment.LEFT);
        canvas.close();
    }
}
