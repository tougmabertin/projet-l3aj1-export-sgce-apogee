// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import apogee.elp.ElpApogee;
import java.util.Iterator;
import com.itextpdf.kernel.colors.Color;
import apogee.liste.ListeApogee;
import com.itextpdf.layout.element.Table;
import com.itextpdf.kernel.colors.DeviceRgb;
import java.io.IOException;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.Document;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import apogee.formation.Formation;

public class GenerateTree
{
    private Formation form;
    
    public GenerateTree(final Formation form) {
        this.form = form;
    }
    
    public void createPdfTree(final String repertoire) throws IOException {
        final PdfDocument pdf = new PdfDocument(new PdfWriter(repertoire));
        final PageXofY event = new PageXofY(pdf);
        pdf.addEventHandler("EndPdfPage", (IEventHandler)event);
        final Document doc = new Document(pdf, PageSize.A3);
        doc.setMargins(40.0f, 50.0f, 40.0f, 50.0f);
        doc.add(new Image(ImageDataFactory.create(this.getClass().getClassLoader().getResource("logo_DESCARTES.jpg"))).setWidth(300.0f).setHeight(80.0f).setMarginBottom(60.0f));
        doc.add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)new Paragraph("Arborescence de : " + this.form.getNom()).setTextAlignment(TextAlignment.CENTER)).setFontSize(16.0f)).setBold()).setMarginBottom(60.0f));
        this.addVetTab(this.form, doc);
        event.writeTotal(pdf);
        doc.close();
    }
    
    public void addVetTab(final Formation form, final Document doc) {
        final Color couleurVet = (Color)new DeviceRgb(181, 193, 225);
        final Table tab = new Table(1);
        float position = 20.0f;
        ((Table)tab.setMarginBottom(10.0f)).setMarginLeft(position);
        tab.addCell(form.getCodeEtp());
        tab.setBackgroundColor(couleurVet);
        doc.add((IBlockElement)tab);
        position += 15.0f;
        for (final ListeApogee liste : form.getListe()) {
            this.addListeTab(liste, doc, position);
        }
    }
    
    public void addListeTab(final ListeApogee liste, final Document doc, float position) {
        final Color couleurListe = (Color)new DeviceRgb(225, 230, 245);
        final Table tab = new Table(1);
        ((Table)tab.setMarginLeft(position)).setMarginBottom(10.0f);
        tab.addCell(liste.getCodeLse());
        tab.setBackgroundColor(couleurListe);
        doc.add((IBlockElement)tab);
        position += 15.0f;
        for (final ElpApogee elp : liste.getListe()) {
            this.addElpTab(elp, doc, position);
        }
    }
    
    public void addElpTab(final ElpApogee elp, final Document doc, float position) {
        final Color couleurElp = (Color)new DeviceRgb(243, 245, 249);
        if (elp != null) {
            final Table tab = new Table(1);
            ((Table)tab.setMarginLeft(position)).setMarginBottom(10.0f);
            tab.addCell(elp.getCodeElp());
            tab.setBackgroundColor(couleurElp);
            doc.add((IBlockElement)tab);
            position += 15.0f;
            for (final ListeApogee liste : elp.getListe()) {
                this.addListeTab(liste, doc, position);
            }
        }
    }
}
