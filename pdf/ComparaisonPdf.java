// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import java.util.Iterator;
import com.itextpdf.layout.element.BlockElement;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.element.Cell;
import java.util.Collection;
import apogee.liste.ListeApogee;
import apogee.elp.ElpApogee;
import java.util.ArrayList;
import com.itextpdf.layout.element.Table;
import java.util.List;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import java.io.IOException;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.layout.Document;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import comparaison.Comparaison;
import apogee.formation.Formation;
import apogee.universite.UniversiteApogee;

public class ComparaisonPdf
{
    private String repertoire;
    private UniversiteApogee univ1;
    private UniversiteApogee univ2;
    
    public ComparaisonPdf(final String repertoire, final UniversiteApogee univ1, final UniversiteApogee univ2) {
        this.repertoire = repertoire;
        this.univ1 = univ1;
        this.univ2 = univ2;
    }
    
    public void createPdf(final Formation formApogee1, final Formation formApogee2) throws IOException {
        final Comparaison compare = new Comparaison(this.univ1, this.univ2);
        final PdfWriter writer = new PdfWriter(this.repertoire);
        final PdfDocument pdf = new PdfDocument(writer);
        final PageXofY event = new PageXofY(pdf);
        pdf.addEventHandler("EndPdfPage", (IEventHandler)event);
        final Document doc = new Document(pdf, PageSize.A3);
        doc.setMargins(20.0f, 50.0f, 20.0f, 50.0f);
        doc.add(new Image(ImageDataFactory.create(this.getClass().getClassLoader().getResource("logo_DESCARTES.jpg"))).setWidth(380.0f).setHeight(100.0f));
        doc.add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph("V\u00e9rification de la conformit\u00e9 de la saisie Apog\u00e9e").setFontSize(24.0f)).setBold()).setTextAlignment(TextAlignment.CENTER)).setMarginTop(100.0f)).setMarginBottom(80.0f));
        this.readingGuide(doc);
        doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
        doc.add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph(formApogee1.getNom()).setMarginBottom(30.0f)).setFontColor(ColorConstants.DARK_GRAY)).setBold()).setFontSize(16.0f)).setMarginTop(30.0f)).setMarginBottom(50.0f));
        this.afficherDiffFormation(formApogee1, formApogee2, doc, compare);
        this.compareDescendants(formApogee1, formApogee2, doc, compare);
        event.writeTotal(pdf);
        doc.close();
    }
    
    public void readingGuide(final Document doc) {
        final Color titleColor = ColorConstants.DARK_GRAY;
        final Color subTitleColor = (Color)new DeviceRgb(57, 101, 177);
        final Color textColor = ColorConstants.BLACK;
        final Color lseColor = (Color)new DeviceRgb(12, 167, 117);
        final Color elpColor = (Color)new DeviceRgb(56, 110, 163);
        final Paragraph separator = new Paragraph(" / ");
        ((Paragraph)((Paragraph)separator.setFontSize(18.0f)).setBold()).setFontColor(ColorConstants.RED);
        final Paragraph subTitle1 = new Paragraph("1) Tableau 1 :");
        ((Paragraph)subTitle1.setFontSize(15.0f)).setBold();
        final Paragraph subTitle2 = new Paragraph("2) Tableau 2 :");
        ((Paragraph)subTitle2.setFontSize(15.0f)).setBold();
        final Paragraph subTitle3 = new Paragraph("3) Tableau 3 : ");
        ((Paragraph)subTitle3.setFontSize(15.0f)).setBold();
        final Paragraph subTitle4 = new Paragraph("4) Tableau 4 : ");
        ((Paragraph)subTitle4.setFontSize(15.0f)).setBold();
        final Paragraph text1 = (Paragraph)new Paragraph("- Donn\u00e9es \u00e9gales :").setBold();
        final Paragraph text2 = (Paragraph)new Paragraph("- Donn\u00e9es in\u00e9gales :").setBold();
        final Paragraph text3 = (Paragraph)new Paragraph("- Descendants manquants :").setBold();
        final Paragraph text4 = (Paragraph)new Paragraph("- Descendants ajout\u00e9s :").setBold();
        final Paragraph text5 = (Paragraph)new Paragraph("- Fils = :").setBold();
        final Paragraph text6 = (Paragraph)new Paragraph("- Fils - :").setBold();
        final Paragraph text7 = (Paragraph)new Paragraph("- Fils + :").setBold();
        final Paragraph title = new Paragraph();
        this.setParagraph(title.add("Guide de Lecture :"), 18.0f, 30.0f, 0.0f, 30.0f, true, titleColor);
        final Paragraph diffParamsForm = new Paragraph();
        this.setParagraph(diffParamsForm.add((IBlockElement)subTitle1).add("Diff\u00e9rences entres les donn\u00e9es des deux formations"), 14.0f, 70.0f, 0.0f, 10.0f, false, subTitleColor);
        final Paragraph equalParams = new Paragraph();
        this.setParagraph(equalParams.add((IBlockElement)text1).add(" Donn\u00e9e SGCE."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph unequalParams = new Paragraph();
        this.setParagraph(unequalParams.add((IBlockElement)text2).add((IBlockElement)new Paragraph(" Donn\u00e9e Apog\u00e9e ")).add((IBlockElement)separator).add((IBlockElement)new Paragraph("Donn\u00e9e SGCE.")), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph diffDescendantsForms = new Paragraph();
        this.setParagraph(diffDescendantsForms.add((IBlockElement)subTitle2).add(" Diff\u00e9rences entres les descendants des deux formations"), 14.0f, 70.0f, 15.0f, 10.0f, false, subTitleColor);
        final Paragraph missingFormDescendants = new Paragraph();
        this.setParagraph(missingFormDescendants.add((IBlockElement)text3).add(" Elps et listes Apog\u00e9e pr\u00e9sents dans SGCE et absents dans Apog\u00e9e."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph addedFormDescendants = new Paragraph();
        this.setParagraph(addedFormDescendants.add((IBlockElement)text4).add(" Elps et listes Apog\u00e9e pr\u00e9sents dans Apog\u00e9e et absents dans SGCE."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph diffParamsLses = new Paragraph();
        this.setParagraph(diffParamsLses.add((IBlockElement)subTitle3).add("Diff\u00e9rences entres les param\u00e8tres des listes Apog\u00e9e communes"), 14.0f, 70.0f, 15.0f, 10.0f, false, subTitleColor);
        final Paragraph commonLseDescendants = new Paragraph();
        this.setParagraph(commonLseDescendants.add((IBlockElement)text5).add(" Descedants Elps et listes Apog\u00e9e appartenants aux deux listes Apog\u00e9e."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph missingLseDescendants = new Paragraph();
        this.setParagraph(missingLseDescendants.add((IBlockElement)text6).add(" Descedants Elps et listes Apog\u00e9e appartenants seulement \u00e0 la liste Apog\u00e9e de la formation SGCE."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph addedLseDescendants = new Paragraph();
        this.setParagraph(addedLseDescendants.add((IBlockElement)text7).add(" Descedants Elps et listes Apog\u00e9e appartenants seulement \u00e0 la liste Apog\u00e9e de la formation Apog\u00e9e."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph diffParamsElps = new Paragraph();
        this.setParagraph(diffParamsElps.add((IBlockElement)subTitle4).add("Diff\u00e9rences entres les param\u00e8tres des Elps communs"), 14.0f, 70.0f, 15.0f, 10.0f, false, subTitleColor);
        final Paragraph commonElpDescendants = new Paragraph();
        this.setParagraph(commonElpDescendants.add((IBlockElement)text5).add("Descedants Elps et listes Apog\u00e9e appartenants aux deux Elps."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph missingElpDescendants = new Paragraph();
        this.setParagraph(missingElpDescendants.add((IBlockElement)text6).add("Descedants Elps et listes Apog\u00e9e appartenants seulement \u00e0 l'Elp de la formation SGCE."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph addedElpDescendants = new Paragraph();
        this.setParagraph(addedElpDescendants.add((IBlockElement)text7).add("Descedants Elps et listes Apog\u00e9e appartenants seulement \u00e0 l'Elp de la formation Apog\u00e9e."), 12.0f, 90.0f, 0.0f, 0.0f, false, textColor);
        final Paragraph p = (Paragraph)((Paragraph)((Paragraph)new Paragraph().setFontSize(15.0f)).setMarginLeft(70.0f)).setMarginTop(30.0f);
        final Paragraph lse = (Paragraph)((Paragraph)new Paragraph("Texte en vert : Liste Apog\u00e9e").setFontColor(lseColor)).setBold();
        final Paragraph elp = (Paragraph)((Paragraph)new Paragraph("Texte en bleu : ELP").setFontColor(elpColor)).setBold();
        p.add((IBlockElement)new Paragraph("<<").setBold()).add(" Indication tableaux 3 et 4 : ").add((IBlockElement)lse).add(" et le ").add((IBlockElement)elp).add(". ").add((IBlockElement)new Paragraph(">>").setBold());
        doc.add((IBlockElement)title).add((IBlockElement)diffParamsForm).add((IBlockElement)equalParams).add((IBlockElement)unequalParams).add((IBlockElement)diffDescendantsForms).add((IBlockElement)missingFormDescendants).add((IBlockElement)addedFormDescendants).add((IBlockElement)diffParamsLses).add((IBlockElement)equalParams).add((IBlockElement)unequalParams).add((IBlockElement)commonLseDescendants).add((IBlockElement)missingLseDescendants).add((IBlockElement)addedLseDescendants).add((IBlockElement)diffParamsElps).add((IBlockElement)equalParams).add((IBlockElement)unequalParams).add((IBlockElement)commonElpDescendants).add((IBlockElement)missingElpDescendants).add((IBlockElement)addedElpDescendants).add((IBlockElement)p);
    }
    
    private void setParagraph(final Paragraph p, final float fontSize, final float left, final float top, final float bottom, final Boolean bold, final Color color) {
        ((Paragraph)((Paragraph)((Paragraph)((Paragraph)p.setFontSize(fontSize)).setMarginLeft(left)).setMarginTop(top)).setMarginBottom(bottom)).setFontColor(color);
        if (bold) {
            p.setBold();
        }
    }
    
    private void afficherDiffFormation(final Formation form1, final Formation form2, final Document doc, final Comparaison compare) {
        final List<String> diffParams = compare.afficherDiffFormation(form1, form2);
        final Table tab = this.getVetTable();
        if (form1 != null && form2 != null) {
            this.fillTable(diffParams, tab);
        }
        doc.add((IBlockElement)tab);
    }
    
    private void compareDescendants(final Formation form1, final Formation form2, final Document doc, final Comparaison compare) {
        final List<ElpApogee> listElps1 = new ArrayList<ElpApogee>();
        final List<ListeApogee> listLses1 = new ArrayList<ListeApogee>();
        compare.getDescendantsFormation(form1, listElps1, listLses1);
        final List<ElpApogee> listElps2 = new ArrayList<ElpApogee>();
        final List<ListeApogee> listLses2 = new ArrayList<ListeApogee>();
        compare.getDescendantsFormation(form2, listElps2, listLses2);
        final List<String> missingElps = new ArrayList<String>();
        final List<String> addedElps = new ArrayList<String>();
        final List<String> missingLses = new ArrayList<String>();
        final List<String> addedLses = new ArrayList<String>();
        final List<String> sharedElps = new ArrayList<String>(compare.getSharedElps(listElps1, listElps2, missingElps, addedElps));
        final List<String> sharedLses = new ArrayList<String>(compare.getSharedLses(listLses1, listLses2, missingLses, addedLses));
        final Table mainTab = new Table(new float[] { 1.0f, 1.0f });
        ((Table)mainTab.setMarginBottom(40.0f)).setMarginTop(20.0f);
        mainTab.addCell((Cell)new Cell(1, 2).add((IBlockElement)((Paragraph)new Paragraph("Diff\u00e9rence Descendants").setFontSize(15.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setBackgroundColor(ColorConstants.GRAY));
        final Table tabOfMissings = new Table(new float[] { 1.0f, 1.0f });
        tabOfMissings.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfMissings.addCell((Cell)((Cell)new Cell(1, 2).add((IBlockElement)new Paragraph(" Descendants manquants ")).setFontSize(14.0f)).setBackgroundColor(ColorConstants.RED));
        final Color lseColor = (Color)new DeviceRgb(181, 193, 225);
        final Color elpColor = (Color)new DeviceRgb(225, 230, 245);
        final Table tabOfMissingElps = new Table(new float[] { 1.0f });
        tabOfMissingElps.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfMissingElps.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("ELPs")).setBackgroundColor(elpColor));
        final Table tabOfMissingLses = new Table(new float[] { 1.0f });
        tabOfMissingLses.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfMissingLses.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("LSEs")).setBackgroundColor(lseColor));
        final Table tabOfAdded = new Table(new float[] { 1.0f, 1.0f });
        tabOfAdded.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfAdded.addCell((Cell)((Cell)new Cell(1, 2).add((IBlockElement)new Paragraph("\tDescendants ajout\u00e9s ")).setFontSize(14.0f)).setBackgroundColor(ColorConstants.GREEN));
        final Table tabOfAddedElps = new Table(new float[] { 1.0f });
        tabOfAddedElps.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfAddedElps.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("ELPs")).setBackgroundColor(elpColor));
        final Table tabOfAddedLses = new Table(new float[] { 1.0f });
        tabOfAddedLses.setHorizontalAlignment(HorizontalAlignment.CENTER);
        tabOfAddedLses.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("LSEs")).setBackgroundColor(lseColor));
        this.fillTable(missingElps, tabOfMissingElps);
        this.fillTable(missingLses, tabOfMissingLses);
        tabOfMissings.addCell((BlockElement)tabOfMissingElps).addCell((BlockElement)tabOfMissingLses);
        this.fillTable(addedElps, tabOfAddedElps);
        this.fillTable(addedLses, tabOfAddedLses);
        tabOfAdded.addCell((BlockElement)tabOfAddedElps).addCell((BlockElement)tabOfAddedLses);
        mainTab.addCell((BlockElement)tabOfMissings).addCell((BlockElement)tabOfAdded);
        doc.add((IBlockElement)mainTab);
        final Table tabLse = this.getLseTable();
        this.fillTabWithLsesDiff(sharedLses, tabLse, compare);
        doc.add((IBlockElement)tabLse);
        final Table tabElp = this.getElpTable();
        this.fillTabWithElpsDiff(sharedElps, tabElp, compare);
        doc.add((IBlockElement)tabElp);
    }
    
    private void fillTabWithLsesDiff(final List<String> listeCodesLses, final Table tab, final Comparaison compare) {
        final List<ElpApogee> listeDescendantsElps1 = new ArrayList<ElpApogee>();
        final List<ElpApogee> listeDescendantsElps2 = new ArrayList<ElpApogee>();
        final List<ListeApogee> listeDescendantsLses1 = new ArrayList<ListeApogee>();
        final List<ListeApogee> listeDescendantsLses2 = new ArrayList<ListeApogee>();
        final List<String> missingLses = new ArrayList<String>();
        final List<String> addedLses = new ArrayList<String>();
        final List<String> missingElps = new ArrayList<String>();
        final List<String> addedElps = new ArrayList<String>();
        List<String> sharedElps = new ArrayList<String>();
        List<String> sharedLses = new ArrayList<String>();
        final Color lseColor = (Color)new DeviceRgb(12, 167, 117);
        final Color elpColor = (Color)new DeviceRgb(56, 110, 163);
        for (final String s : listeCodesLses) {
            final ListeApogee lse1 = this.univ1.getListeApogeeByCode(s);
            final ListeApogee lse2 = this.univ2.getListeApogeeByCode(s);
            compare.getDescendantsListeApogee(lse1, listeDescendantsElps1, listeDescendantsLses1);
            compare.getDescendantsListeApogee(lse2, listeDescendantsElps2, listeDescendantsLses2);
            sharedElps = compare.getSharedElps(listeDescendantsElps1, listeDescendantsElps2, missingElps, addedElps);
            sharedLses = compare.getSharedLses(listeDescendantsLses1, listeDescendantsLses2, missingLses, addedLses);
            this.fillTable(compare.afficheDiffLse(lse1, lse2), tab);
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(sharedLses, lseColor)).add((IBlockElement)this.getParagraphFromList(sharedElps, elpColor)));
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(missingLses, lseColor)).add((IBlockElement)this.getParagraphFromList(missingElps, elpColor)));
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(addedLses, lseColor)).add((IBlockElement)this.getParagraphFromList(addedElps, elpColor)));
            listeDescendantsElps1.clear();
            listeDescendantsElps2.clear();
            listeDescendantsLses1.clear();
            listeDescendantsLses2.clear();
            missingLses.clear();
            missingElps.clear();
            addedLses.clear();
            addedElps.clear();
        }
    }
    
    private void fillTabWithElpsDiff(final List<String> listeCodesElps, final Table tab, final Comparaison compare) {
        final List<ElpApogee> listeDescendantsElps1 = new ArrayList<ElpApogee>();
        final List<ElpApogee> listeDescendantsElps2 = new ArrayList<ElpApogee>();
        final List<ListeApogee> listeDescendantsLses1 = new ArrayList<ListeApogee>();
        final List<ListeApogee> listeDescendantsLses2 = new ArrayList<ListeApogee>();
        final List<String> missingLses = new ArrayList<String>();
        final List<String> addedLses = new ArrayList<String>();
        final List<String> missingElps = new ArrayList<String>();
        final List<String> addedElps = new ArrayList<String>();
        List<String> sharedElps = new ArrayList<String>();
        List<String> sharedLses = new ArrayList<String>();
        final Color lseColor = (Color)new DeviceRgb(12, 167, 117);
        final Color elpColor = (Color)new DeviceRgb(56, 110, 163);
        for (final String s : listeCodesElps) {
            final ElpApogee elp1 = this.univ1.getElpByCode(s);
            final ElpApogee elp2 = this.univ2.getElpByCode(s);
            compare.getDescendantsElp(elp1, listeDescendantsElps1, listeDescendantsLses1);
            compare.getDescendantsElp(elp2, listeDescendantsElps2, listeDescendantsLses2);
            sharedElps = compare.getSharedElps(listeDescendantsElps1, listeDescendantsElps2, missingElps, addedElps);
            sharedLses = compare.getSharedLses(listeDescendantsLses1, listeDescendantsLses2, missingLses, addedLses);
            this.fillTable(compare.afficheDiffElp(elp1, elp2), tab);
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(sharedLses, lseColor)).add((IBlockElement)this.getParagraphFromList(sharedElps, elpColor)));
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(missingLses, lseColor)).add((IBlockElement)this.getParagraphFromList(missingElps, elpColor)));
            tab.addCell((BlockElement)new Paragraph().add((IBlockElement)this.getParagraphFromList(addedLses, lseColor)).add((IBlockElement)this.getParagraphFromList(addedElps, elpColor)));
            listeDescendantsElps1.clear();
            listeDescendantsElps2.clear();
            listeDescendantsLses1.clear();
            listeDescendantsLses2.clear();
            missingLses.clear();
            missingElps.clear();
            addedLses.clear();
            addedElps.clear();
        }
    }
    
    private Paragraph getParagraphFromList(final List<String> liste, final Color color) {
        final Paragraph p = new Paragraph();
        for (final String s : liste) {
            ((Paragraph)((Paragraph)p.add(String.valueOf(s) + "\n").setFontColor(color)).setFontSize(10.0f)).setBold();
        }
        return p;
    }
    
    private void fillTable(final List<String> list, final Table tab) {
        for (final String s : list) {
            tab.addCell((BlockElement)this.getParagraphFromString(s));
        }
    }
    
    private Paragraph getParagraphFromString(final String s) {
        final Paragraph p = new Paragraph();
        p.setFontSize(10.0f);
        if (s.contains(" / ")) {
            final String s2 = String.valueOf(s.substring(0, s.indexOf("/") - 1)) + " ";
            final String separ = s.substring(s.indexOf("/") - 1, s.indexOf("/") + 1);
            final String s3 = s.substring(s.indexOf("/") + 1, s.length());
            p.add(s2).add((IBlockElement)((Paragraph)((Paragraph)new Paragraph(separ).setFontSize(18.0f)).setBold()).setFontColor(ColorConstants.RED)).add(s3);
            return p;
        }
        return p.add(s);
    }
    
    private Table getVetTable() {
        final Table tab = new Table(new float[] { 1.0f, 1.0f, 8.0f, 6.0f });
        ((Table)((Table)tab.setWidth(760.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setMarginBottom(50.0f);
        tab.addCell((Cell)((Cell)((Cell)new Cell(1, 4).add((IBlockElement)new Paragraph("Conformit\u00e9 des param\u00e8tres de la formation ")).setBackgroundColor(ColorConstants.GRAY)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setFontSize(14.0f));
        tab.addCell(this.getTabColName("Code"));
        tab.addCell(this.getTabColName("Version"));
        tab.addCell(this.getTabColName("Nom"));
        tab.addCell(this.getTabColName("Acronyme"));
        return tab;
    }
    
    private Table getLseTable() {
        final Table tab = new Table(new float[] { 1.0f, 4.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f });
        ((Table)((Table)((Table)tab.setWidth(760.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setMarginBottom(40.0f)).setMarginTop(20.0f);
        tab.addCell((Cell)((Cell)((Cell)new Cell(1, 8).add((IBlockElement)new Paragraph("Conformit\u00e9 des param\u00e8tres des LSEs communes")).setBackgroundColor(ColorConstants.GRAY)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setFontSize(14.0f));
        tab.addCell(this.getTabColName("Code Liste"));
        tab.addCell(this.getTabColName("Type"));
        tab.addCell(this.getTabColName("Nom"));
        tab.addCell(this.getTabColName("choix min"));
        tab.addCell(this.getTabColName("choix max"));
        tab.addCell(this.getTabColName("Fils ="));
        tab.addCell(this.getTabColName("Fils -"));
        tab.addCell(this.getTabColName("Fils +"));
        tab.setKeepTogether(true);
        return tab;
    }
    
    private Table getElpTable() {
        final Table tab = new Table(new float[] { 1.0f, 1.0f, 4.0f, 2.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f });
        ((Table)((Table)((Table)tab.setWidth(760.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setMarginBottom(40.0f)).setMarginTop(20.0f);
        tab.addCell((Cell)((Cell)((Cell)new Cell(1, 14).add((IBlockElement)new Paragraph("Conformit\u00e9 des param\u00e8tres des ELPs communs")).setBackgroundColor(ColorConstants.GRAY)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setFontSize(14.0f));
        tab.addCell(this.getTabColName("Code"));
        tab.addCell(this.getTabColName("Type"));
        tab.addCell(this.getTabColName("Nom"));
        tab.addCell(this.getTabColName("Acronyme"));
        tab.addCell(this.getTabColName("Ects"));
        tab.addCell(this.getTabColName("CM"));
        tab.addCell(this.getTabColName("TD"));
        tab.addCell(this.getTabColName("TP"));
        tab.addCell(this.getTabColName("Projet"));
        tab.addCell(this.getTabColName("Stage"));
        tab.addCell(this.getTabColName("TDM"));
        tab.addCell(this.getTabColName("Fils ="));
        tab.addCell(this.getTabColName("Fils -"));
        tab.addCell(this.getTabColName("Fils +"));
        return tab;
    }
    
    private Cell getTabColName(final String text) {
        final Cell cell = new Cell();
        cell.add((IBlockElement)new Paragraph().add(text).setFontSize(13.0f)).setBackgroundColor(ColorConstants.LIGHT_GRAY);
        return cell;
    }
}
