// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.element.Cell;
import sgce.elps.Ecue;
import sgce.elps.BlocSousChoix;
import java.util.Collection;
import java.util.ArrayList;
import sgce.elps.Ue;
import sgce.elps.Obligatoire;
import sgce.elps.BlocChoix;
import sgce.liste.Semestre;
import java.util.Iterator;
import sgce.elps.Parcours;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.BlockElement;
import sgce.elps.Formation;
import com.itextpdf.layout.element.Table;

public class FillWithSgce
{
    public void fillWithFormation(final Table table, final Formation form) {
        table.addCell(form.getMention().getCode());
        table.addCell(String.valueOf(form.getCode()) + " - v" + form.getVersion());
        table.addCell("VET");
        table.addCell((BlockElement)((Paragraph)((Paragraph)new Paragraph(form.getAcronyme()).setFontColor(ColorConstants.BLACK)).setBold()).setFontSize(15.0f));
        this.addEmptyCell(table, 8);
        for (final Parcours p : form.getListeParcours()) {
            this.fillWithParcours(table, p);
        }
    }
    
    public void fillWithParcours(final Table table, final Parcours parcours) {
        table.addCell(parcours.getFormation().getCode());
        table.addCell("  " + parcours.getCode());
        table.addCell("PAR");
        table.addCell((BlockElement)((Paragraph)((Paragraph)((Paragraph)new Paragraph(parcours.getNom()).setMarginLeft(10.0f)).setFontSize(14.0f)).setFontColor(ColorConstants.DARK_GRAY)).setBold());
        this.addEmptyCell(table, 8);
        for (final Semestre s : parcours.getListeSemestre().getListe()) {
            this.fillWithSemestre(table, s);
        }
    }
    
    public void fillWithSemestre(final Table table, final Semestre sem) {
        table.addCell(sem.getCodePere());
        table.addCell("");
        table.addCell("SEM");
        table.addCell((BlockElement)((Paragraph)((Paragraph)new Paragraph(sem.getNom()).setMarginLeft(20.0f)).setFontSize(13.0f)).setBold());
        table.addCell(new StringBuilder().append(sem.getEcts()).toString());
        this.addEmptyCell(table, 7);
        for (final BlocChoix bc : sem.getListeBc().getListe()) {
            this.fillWithBlocC(table, bc);
        }
    }
    
    public void fillWithBlocC(final Table table, final BlocChoix bc) {
        if (bc.getObligatoire().equals(Obligatoire.OC)) {
            table.addCell("");
            if (bc.getCode() != null) {
                table.addCell(bc.getCode());
            }
            else {
                table.addCell("");
            }
            table.addCell("Bloc");
            table.addCell((BlockElement)new Paragraph(String.valueOf(bc.getNom()) + "  " + "(" + bc.getNbUes() + " au choix)").setMarginLeft(30.0f));
            this.addEmptyCell(table, 8);
        }
        else if (bc.getObligatoire().equals(Obligatoire.F)) {
            table.addCell("");
            if (bc.getCode() != null) {
                table.addCell(bc.getCode());
            }
            else {
                table.addCell("");
            }
            table.addCell("Bloc");
            table.addCell((BlockElement)new Paragraph("Facultatif").setMarginLeft(30.0f));
            this.addEmptyCell(table, 8);
        }
        for (final Ue ue : bc.getListeUe().getListe()) {
            this.fillWithUe(table, ue);
        }
    }
    
    public void fillWithUe(final Table table, final Ue ue) {
        if (ue.getBlocChoix().getCode() != null) {
            table.addCell(ue.getBlocChoix().getCode());
        }
        else {
            table.addCell("");
        }
        table.addCell(ue.getCode());
        table.addCell("UE");
        if (ue.getBlocChoix().getObligatoire() == Obligatoire.O) {
            table.addCell((BlockElement)((Paragraph)new Paragraph(ue.getNom()).setMarginLeft(30.0f)).setFontSize(13.0f));
        }
        else {
            table.addCell((BlockElement)((Paragraph)new Paragraph(ue.getNom()).setMarginLeft(40.0f)).setFontSize(12.0f));
        }
        table.addCell(new StringBuilder().append(ue.getEcts()).toString());
        final ArrayList<String> heures = new ArrayList<String>(ue.getHeuresCours().values());
        for (final String h : heures) {
            table.addCell(h);
        }
        table.addCell(ue.getCnu());
        final ArrayList<BlocSousChoix> listeBsc = ue.getListeBsc().getListe();
        if (listeBsc.size() > 1 || (listeBsc.size() == 1 && listeBsc.get(0).getListeEcue().getListe().size() > 1)) {
            for (final BlocSousChoix bsc : ue.getListeBsc().getListe()) {
                for (final Ecue ecue : bsc.getListeEcue().getListe()) {
                    this.fillWithEcue(table, ecue);
                }
            }
        }
    }
    
    public void fillWithEcue(final Table table, final Ecue ecue) {
        table.addCell(ecue.getBlocSousChoix().getUe().getCode());
        table.addCell(ecue.getCode());
        table.addCell("ECUE");
        table.addCell((BlockElement)((Paragraph)new Paragraph(ecue.getNom()).setMarginLeft(40.0f)).setFontSize(12.0f));
        table.addCell(new StringBuilder().append(ecue.getEcts()).toString());
        final ArrayList<String> heures = new ArrayList<String>(ecue.getHeuresCours().values());
        for (final String h : heures) {
            table.addCell(h);
        }
        table.addCell(ecue.getCnu());
    }
    
    private void addEmptyCell(final Table tab, final int n) {
        for (int i = 0; i < n; ++i) {
            tab.addCell(new Cell());
        }
    }
    
    public Table getTable() {
        final Table tab = new Table(new float[] { 1.0f, 1.0f, 1.0f, 8.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, 2.0f });
        ((Table)((Table)tab.setWidth(760.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setMarginBottom(40.0f);
        tab.addCell(this.getTabColName("Liste"));
        tab.addCell(this.getTabColName("Code"));
        tab.addCell(this.getTabColName("Type"));
        tab.addCell(this.getTabColName("Nom"));
        tab.addCell(this.getTabColName("Ects"));
        tab.addCell(this.getTabColName("CM"));
        tab.addCell(this.getTabColName("TD"));
        tab.addCell(this.getTabColName("TP"));
        tab.addCell(this.getTabColName("Projet"));
        tab.addCell(this.getTabColName("Stage"));
        tab.addCell(this.getTabColName("TDM"));
        tab.addCell(this.getTabColName("CNU"));
        return tab;
    }
    
    private Cell getTabColName(final String text) {
        final Cell cell = new Cell();
        cell.add((IBlockElement)new Paragraph().add(text).setFontSize(14.0f)).setBackgroundColor(ColorConstants.GRAY);
        return cell;
    }
}
