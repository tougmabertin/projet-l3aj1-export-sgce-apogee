// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import apogee.elp.TypeElp;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.element.BlockElement;
import apogee.elp.ElpApogee;
import java.util.Iterator;
import com.itextpdf.kernel.colors.Color;
import apogee.liste.ListeApogee;
import com.itextpdf.kernel.pdf.PdfOutline;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import apogee.formation.Formation;
import com.itextpdf.layout.element.Table;
import apogee.universite.UniversiteApogee;

public class FillWithApogee
{
    private UniversiteApogee universite;
    
    public FillWithApogee(final UniversiteApogee universite) {
        this.universite = universite;
    }
    
    public void fillWithVet(final Table tab, final Formation form, final PdfDocument pdf) {
        final Color couleurVet = (Color)new DeviceRgb(181, 193, 225);
        final float marge = 10.0f;
        final float fontSize = 15.0f;
        final PdfOutline root = pdf.getOutlines(false);
        tab.addCell(new Cell());
        tab.addCell(String.valueOf(form.getCodeEtp()) + "-" + form.getVersion());
        tab.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("VET").setBold()).setBackgroundColor(couleurVet));
        tab.addCell((Cell)new Cell().add((IBlockElement)((Paragraph)((Paragraph)new Paragraph(form.getNom()).setFontColor(ColorConstants.BLACK)).setBold()).setFontSize(fontSize)).setBackgroundColor(couleurVet));
        this.addEmptyCell(tab, 7);
        final PdfOutline outlineCodeVet = root.addOutline("VET - " + form.getCodeEtp());
        outlineCodeVet.setStyle(PdfOutline.FLAG_BOLD);
        outlineCodeVet.setColor(ColorConstants.BLACK);
        for (final ListeApogee liste : form.getListe()) {
            if (liste != null) {
                liste.setCodePere(form.getCodeEtp());
            }
            this.fillWithListeApogee(tab, liste, marge, fontSize, outlineCodeVet);
        }
    }
    
    public void fillWithElp(final Table tab, final ElpApogee elp, float marge, float fontSize, final PdfOutline outline) {
        final Color couleurPar = (Color)new DeviceRgb(225, 230, 245);
        final Color couleurSem = (Color)new DeviceRgb(243, 245, 249);
        if (elp != null) {
            tab.addCell(new StringBuilder().append(elp.getCodePere()).toString());
            tab.addCell(elp.getCodeElp());
            Label_0763: {
                switch (elp.getType()) {
                    case PARCOURS: {
                        tab.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("PAR").setBold()).setBackgroundColor(couleurPar));
                        tab.addCell((Cell)new Cell().add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize)).setFontColor(ColorConstants.DARK_GRAY)).setBold()).setBackgroundColor(couleurPar));
                        tab.addCell(elp.getEcts());
                        this.addEmptyCell(tab, 6);
                        break Label_0763;
                    }
                    case SEMESTRE: {
                        tab.addCell((Cell)new Cell().add((IBlockElement)new Paragraph("SEM").setBold()).setBackgroundColor(couleurSem));
                        tab.addCell((Cell)new Cell().add((IBlockElement)((Paragraph)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize)).setBold()).setBackgroundColor(couleurSem));
                        tab.addCell(elp.getEcts());
                        this.addEmptyCell(tab, 6);
                        break Label_0763;
                    }
                    case BLOC: {
                        tab.addCell("BLOC");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        this.addEmptyCell(tab, 7);
                        break Label_0763;
                    }
                    case UE: {
                        tab.addCell("UE");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        tab.addCell(elp.getEcts());
                        this.addHeures(tab, elp, this.universite);
                        break Label_0763;
                    }
                    case UF: {
                        tab.addCell("UF");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        tab.addCell(elp.getEcts());
                        this.addHeures(tab, elp, this.universite);
                        break Label_0763;
                    }
                    case ECUE: {
                        tab.addCell("ECUE");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        tab.addCell(elp.getEcts());
                        this.addHeures(tab, elp, this.universite);
                        break Label_0763;
                    }
                    case MATIERE: {
                        tab.addCell("MATIERE");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        tab.addCell(elp.getEcts());
                        this.addHeures(tab, elp, this.universite);
                        break Label_0763;
                    }
                    case ENS: {
                        tab.addCell("ENS");
                        tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                        tab.addCell(elp.getEcts());
                        this.addHeures(tab, elp, this.universite);
                        break;
                    }
                }
                tab.addCell("ECUE");
                tab.addCell((BlockElement)((Paragraph)new Paragraph(elp.getNom()).setMarginLeft(marge)).setFontSize(fontSize));
                tab.addCell(elp.getEcts());
                this.addHeures(tab, elp, this.universite);
            }
            final PdfOutline outlineCodeElp = outline.addOutline("ELP - " + elp.getType() + " : " + elp.getCodeElp());
            outlineCodeElp.setColor(ColorConstants.GRAY);
            outlineCodeElp.setOpen(false);
            for (final ListeApogee liste : elp.getListe()) {
                if (liste != null) {
                    liste.setCodePere(elp.getCodePere());
                }
                marge += 15.0f;
                --fontSize;
                this.fillWithListeApogee(tab, liste, marge, fontSize, outlineCodeElp);
            }
        }
    }
    
    public void fillWithListeApogee(final Table tab, final ListeApogee liste, final float marge, final float fontSize, final PdfOutline outline) {
        final String codeP = liste.getCodeLse();
        final PdfOutline outlineCodeLse = outline.addOutline("LSE - " + liste.getCodeLse());
        outlineCodeLse.setColor(ColorConstants.DARK_GRAY);
        outlineCodeLse.setOpen(false);
        for (final ElpApogee elp : liste.getListe()) {
            if (elp != null) {
                elp.setCodePere(codeP);
            }
            this.fillWithElp(tab, elp, marge, fontSize, outlineCodeLse);
        }
    }
    
    private void addEmptyCell(final Table tab, final int n) {
        for (int i = 0; i < n; ++i) {
            tab.addCell(new Cell());
        }
    }
    
    public Table getTable() {
        final Table tab = new Table(new float[] { 1.0f, 1.0f, 1.0f, 8.0f, 1.0f, 1.0f, 1.0f, 1.0f, 2.0f, 2.0f, 2.0f });
        ((Table)((Table)tab.setWidth(760.0f)).setHorizontalAlignment(HorizontalAlignment.CENTER)).setMarginBottom(40.0f);
        tab.addCell(this.getTabColName("Liste"));
        tab.addCell(this.getTabColName("Code"));
        tab.addCell(this.getTabColName("Type"));
        tab.addCell(this.getTabColName("Nom"));
        tab.addCell(this.getTabColName("Ects"));
        tab.addCell(this.getTabColName("CM"));
        tab.addCell(this.getTabColName("TD"));
        tab.addCell(this.getTabColName("TP"));
        tab.addCell(this.getTabColName("Projet"));
        tab.addCell(this.getTabColName("Stage"));
        tab.addCell(this.getTabColName("TDM"));
        return tab;
    }
    
    private Cell getTabColName(final String text) {
        final Cell cell = new Cell();
        cell.add((IBlockElement)new Paragraph().add(text).setFontSize(14.0f)).setBackgroundColor(ColorConstants.GRAY);
        return cell;
    }
    
    private void addHeures(final Table tab, final ElpApogee elp, final UniversiteApogee universite) {
        if (elp.getHeures() != null) {
            tab.addCell(elp.getHeures().getNbreCM());
            tab.addCell(elp.getHeures().getNbreTD());
            tab.addCell(elp.getHeures().getNbreTP());
            tab.addCell(elp.getHeures().getNbreProjet());
            tab.addCell(elp.getHeures().getNbreStage());
            tab.addCell(elp.getHeures().getNbreTdm());
        }
        else {
            this.addEmptyCell(tab, 6);
        }
    }
}
