// 
// Decompiled by Procyon v0.5.36
// 

package pdf;

import com.itextpdf.layout.element.Table;
import transformation.SgceToApogee;
import apogee.universite.UniversiteApogee;
import java.io.IOException;
import java.util.Iterator;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.kernel.pdf.navigation.PdfDestination;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.navigation.PdfExplicitDestination;
import sgce.elps.Formation;
import sgce.elps.TypeMention;
import sgce.elps.Mention;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.IBlockElement;
import com.itextpdf.layout.element.Image;
import com.itextpdf.io.image.ImageDataFactory;
import java.util.ArrayList;
import com.itextpdf.layout.Document;
import sgce.universite.UniversiteSgce;
import sgce.elps.Departement;

public class DocumentForm
{
    private Departement dept;
    private UniversiteSgce universite;
    private Document doc;
    
    public DocumentForm(final Document doc, final Departement dept, final UniversiteSgce universite) {
        this.dept = dept;
        this.universite = universite;
        this.doc = doc;
    }
    
    public void setSummary(final ArrayList<Integer> numPagesListe) throws IOException {
        this.doc.add(new Image(ImageDataFactory.create(this.getClass().getClassLoader().getResource("logo_DESCARTES.jpg"))).setWidth(380.0f).setHeight(100.0f));
        this.doc.add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph("Document d'aide \u00e0 la saisie dans Apog\u00e9e").setFontSize(24.0f)).setBold()).setTextAlignment(TextAlignment.CENTER)).setMarginTop(100.0f)).setMarginBottom(100.0f));
        final Paragraph deptNom = (Paragraph)((Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph(this.dept.getNom()).setFontSize(20.0f)).setFontColor(ColorConstants.DARK_GRAY)).setBold()).setMarginLeft(40.0f)).setMarginBottom(30.0f);
        this.doc.add((IBlockElement)deptNom);
        int i = 0;
        String typeM = null;
        for (final Mention m : this.universite.getDepartementById(this.dept.getId()).getListeMention()) {
            if (m.getType() == TypeMention.L || m.getType() == TypeMention.LP) {
                typeM = "Licence ";
            }
            else {
                typeM = "Master ";
            }
            final Paragraph mNom = (Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph(String.valueOf(typeM) + m.getLibelle()).setMarginLeft(80.0f)).setBold()).setFontSize(16.0f)).setFontColor(ColorConstants.GRAY);
            this.doc.add((IBlockElement)mNom);
            for (final Formation f : this.universite.getMentionById(m.getId()).getListeFormation()) {
                final Paragraph fNom = (Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph(f.getNom()).setMarginLeft(120.0f)).setMarginRight(80.0f)).setBackgroundColor(ColorConstants.LIGHT_GRAY)).setFontSize(16.0f);
                if (numPagesListe.size() > 0) {
                    fNom.setAction(PdfAction.createGoTo((PdfDestination)PdfExplicitDestination.createFitBH((int)numPagesListe.get(i), 1200.0f)));
                    ++i;
                }
                this.doc.add((IBlockElement)fNom);
            }
        }
        this.doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }
    
    public void fillDocument(final ArrayList<Integer> numPagesListe) {
        final UniversiteApogee univApogee = new UniversiteApogee();
        final SgceToApogee transform = new SgceToApogee(univApogee);
        final UniversiteApogee univ = transform.transformerArborescence(this.universite);
        final FillWithApogee fillApogee = new FillWithApogee(univ);
        final FillWithSgce fillSgce = new FillWithSgce();
        int nbForms = 0;
        int nbMentions = 0;
        for (final Mention m : this.dept.getListeMention()) {
            ++nbMentions;
            for (final Formation f : m.getListeFormation()) {
                ++nbForms;
            }
        }
        int i = 0;
        for (final Mention j : this.universite.getDepartementById(this.dept.getId()).getListeMention()) {
            this.doc.add((IBlockElement)((Paragraph)((Paragraph)((Paragraph)((Paragraph)new Paragraph("Licence " + j.getLibelle()).setMarginTop(10.0f)).setMarginBottom(50.0f)).setFontSize(16.0f)).setFontColor(ColorConstants.GRAY)).setBold());
            for (final Formation f2 : this.universite.getMentionById(j.getId()).getListeFormation()) {
                ++i;
                final Table tabApogee = fillApogee.getTable();
                final apogee.formation.Formation form = transform.transformationFormation(f2);
                numPagesListe.add(this.doc.getPdfDocument().getNumberOfPages());
                fillApogee.fillWithVet(tabApogee, form, this.doc.getPdfDocument());
                this.doc.add((IBlockElement)tabApogee);
                if (i != nbForms) {
                    this.doc.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                }
            }
        }
    }
}
